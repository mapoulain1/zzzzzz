﻿using System;
using Urho;

namespace ZZZZZZ.Desktop
{
    public class Program
    {
        private static GameManager gameManager;
        static void Main(string[] args)
        {
            var options = new ApplicationOptions("Data")
            {
                LimitFps = true,
                WindowedMode = true,
                ResizableWindow = true,
                Width = (int)(2560 / 3f),
                Height = (int)(1600 / 3f),
                TouchEmulation = true,
            };

            try
            {
                gameManager = new GameManager(options);
                gameManager.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
