@echo off
set /a count=0
for /r %%v in (*.tmx) do (set /a count=count+1)
echo %count% file(s) found

for /r %%v in (*.tmx) do sed -i -e "s/<map version=\"1.5\"/<map version=\"1.0\"/g" %%v

for /r %%v in (sed*) do rm %%v
echo Done !

set /p DUMMY=

