<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="interaction" tilewidth="16" tileheight="16" tilecount="6" columns="6">
 <transformations hflip="0" vflip="0" rotate="0" preferuntransformed="1"/>
 <image source="interaction.png" width="96" height="16"/>
 <tile id="0">
  <properties>
   <property name="Spike" value="East"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="Spike" value="South"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="Spike" value="West"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="Spike" value="North"/>
  </properties>
 </tile>
</tileset>
