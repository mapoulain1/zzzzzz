﻿using Urho;
using Urho.Urho2D;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Graphics
{
    public class ViewportCustom : Viewport
    {
        public ViewportCustom() : base(Application.Current.Context, GameManager.Current.Scene, GameManager.Current.Camera)
        {

            Application.Current.Renderer.SetViewport(0, this);
            Application.Current.Renderer.HDRRendering = true;


            foreach (string postProcess in AppConfig.Current.PostProcessing)
            {
                RenderPath.Append(Application.Current.ResourceCache.GetXmlFile(postProcess));
            }

            Application.Current.Engine.PostRenderUpdate += Engine_PostRenderUpdate;
            Application.Current.Engine.MaxFps = 60;

        }

        private void Engine_PostRenderUpdate(PostRenderUpdateEventArgs obj)
        {
            if (AppConfig.Current.DrawDebug)
            {
                Application.Current.Renderer.DrawDebugGeometry(true);
                var debugRendererComp = Scene.GetComponent<DebugRenderer>();
                Scene.GetComponent<PhysicsWorld2D>()?.DrawDebugGeometry(debugRendererComp, false);
            }
        }
    }
}
