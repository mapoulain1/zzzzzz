﻿using Urho;
using Urho.Actions;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Graphics
{
    public class CameraCustom : Camera
    {
        public CameraCustom() : base()
        {
            GameManager.Current.Scene.CreateChild().AddComponent(this);

            Orthographic = true;
            Zoom = 4.2f;
            Node.Position = new Vector3(3.85f, 2.4f, -1f);
        }
    }
}
