﻿using Urho;
using ZZZZZZ.Graphics;
using ZZZZZZ.GUI;
using ZZZZZZ.GUI.Navigations;
using ZZZZZZ.Particles;
using ZZZZZZ.Physics;
using ZZZZZZ.Player;
using ZZZZZZ.Server;
using ZZZZZZ.Sounds;
using ZZZZZZ.Utils;
using ZZZZZZ.WebAPIs;
using ZZZZZZ.Worlds;

namespace ZZZZZZ
{
    public partial class GameManager : Application
    {
        public static new GameManager Current { get => Application.Current as GameManager; }

        public ScenePhysics Scene { get; private set; }
        public CameraCustom Camera { get; private set; }
        public Viewport Viewport { get; private set; }
        public GUIFps FPS { get; private set; }
        public ExtraFunction ExtraFunction { get; private set; }
        public LocalPlayerManager LocalPlayerManager { get; set; }
        public RemotePlayerManager RemotePlayerManager { get; set; }
        public IWebAPI API { get; private set; }
        public LevelManager LevelManager { get; private set; }
        public Navigation Navigation { get; private set; }
        public MusicManager MusicManager { get; private set; }
        public SoundManager SoundManager { get; private set; }
        public BackgroundParticleEmitter BackgroundParticle { get; private set; }

        public GameManager() : this(null){ }
        public GameManager(ApplicationOptions options) : base(options) { }

        protected override void Setup()
        {
            base.Setup();
            UnhandledException += GameManager_UnhandledException;
            ResourceCache.ResourceNotFound += ResourceCache_ResourceNotFound;
        }

        private void ResourceCache_ResourceNotFound(Urho.Resources.ResourceNotFoundEventArgs obj) => System.Console.WriteLine($"Unable to find {obj.ResourceName}");
        private void GameManager_UnhandledException(object sender, UnhandledExceptionEventArgs e) => System.Console.WriteLine($"Unhandled exception from {sender} : {e.Exception.Message}");


        protected override void Start()
        {
            base.Start();
            Graphics.WindowTitle = $"ZZZZZZ - {AppConfig.Current.Version}";

            Scene = new ScenePhysics();
            Camera = new CameraCustom();
            Viewport = new ViewportCustom();
            FPS = new GUIFps();
            ExtraFunction = new ExtraFunction();
            MusicManager = new MusicManager();
            SoundManager = new SoundManager();

            Navigation = Navigation.Current;
            Navigation.Load(Assets.UI.Menus.Intro);

            MusicManager.Play(MusicType.Menu);

            BackgroundParticle = new BackgroundParticleEmitter();
            BackgroundParticle.Start();

            LevelManager = new LevelManager(Scene.CollisionHandler);
            Input.KeyDown += Input_KeyDown;

        }

        private void Input_KeyDown(KeyDownEventArgs obj)
        {
            if(obj.Key == Key.P)
            {
                LocalPlayerManager.Character.Move(Vector2.One);
            }
        }
    }
}
