﻿using ZZZZZZ.Entities.Character;
using ZZZZZZ.Physics;

namespace ZZZZZZ.Inputs
{
    public abstract class AbstractInput
    {
        public bool IsConnected { get; protected set; }
        public virtual Movement Movement { get; protected set; }
        public virtual GravityDirection Gravity { get; set; }
        public virtual bool Visible { get; set; }
        public AbstractInput()
        {
            IsConnected = false;
        }

        
    }
}
