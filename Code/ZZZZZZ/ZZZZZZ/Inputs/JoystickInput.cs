﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Gui;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.GUI.Inputs;
using ZZZZZZ.Physics;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Inputs
{
    public class JoystickInput : AbstractInput
    {
        public override bool Visible
        {
            get => visible;
            set
            {
                visible = value;
                if(rightJoystick != null) rightJoystick.Visible = value;
                if (leftJoystick != null) leftJoystick.Visible = value;
            }
        }

        private readonly OnScreenJoystick rightJoystick;
        private readonly OnScreenJoystick leftJoystick;
        private GravityDirection oldGravity;
        private bool visible;
        public override Movement Movement
        {
            get
            {
                if (leftJoystick.LockedY && leftJoystick.X < -0.5f || leftJoystick.LockedX && leftJoystick.Y < -0.5f)
                    return Movement.Left;
                if (leftJoystick.LockedY && leftJoystick.X > 0.5f || leftJoystick.LockedX && leftJoystick.Y > 0.5f)
                    return Movement.Right;
                return Movement.None;
            }
        }


        public override GravityDirection Gravity
        {
            get
            {
                if (rightJoystick.X < -0.5f)
                    oldGravity = GravityDirection.Gauche;
                else if (rightJoystick.X > 0.5f)
                    oldGravity = GravityDirection.Droite;
                else if (rightJoystick.Y < -0.5f)
                    oldGravity = GravityDirection.Haut;
                else if (rightJoystick.Y > 0.5f)
                    oldGravity = GravityDirection.Bas;
                leftJoystick.LockedY = oldGravity == GravityDirection.Haut || oldGravity == GravityDirection.Bas;
                leftJoystick.LockedX = oldGravity == GravityDirection.Droite || oldGravity == GravityDirection.Gauche;

                return oldGravity;
            }
        }



        public JoystickInput()
        {
            Visible = true;
            IsConnected = true;
            leftJoystick = new OnScreenJoystick(ToolBox.ScreenPosition(0.05f, 0.7f))
            {
                LockedY = true,
            };

            rightJoystick = new OnScreenJoystick(ToolBox.ScreenPosition(0.825f, 0.7f));
            oldGravity = GameManager.Current.Scene.Gravity;
        }

    }
}
