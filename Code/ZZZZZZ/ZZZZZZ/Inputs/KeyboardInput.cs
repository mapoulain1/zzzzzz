﻿using Urho;
using ZZZZZZ.Entities.Character;

namespace ZZZZZZ.Inputs
{
    class KeyboardInput : AbstractInput
    {
        int MovementCount { get; set; } //nul = none, <0 left, >0 Right
        public KeyboardInput()
        {
            IsConnected = true;
            Application.Current.Input.KeyDown += Input_KeyDown;
            Application.Current.Input.KeyUp += Input_KeyUp;
            MovementCount = 0;
        }

        private void Input_KeyUp(KeyUpEventArgs obj)
        {

            if ((obj.Key == Key.S || obj.Key == Key.D) && MovementCount == 1)MovementCount = 0 ;
            else if ((obj.Key == Key.Q || obj.Key == Key.Z) && MovementCount == -1) MovementCount = 0;

            if (MovementCount == 0) Movement = Movement.None;
            //System.Console.WriteLine(MovementCount);

        }

        private void Input_KeyDown(KeyDownEventArgs obj)
        {
            if (obj.Key == Key.S || obj.Key == Key.D) MovementCount = 1;
            else if (obj.Key == Key.Q || obj.Key == Key.Z) MovementCount = -1;


            if (MovementCount > 0) Movement = Movement.Right;
            else if (MovementCount == 0) Movement = Movement.None;
            else Movement = Movement.Left;
            //System.Console.WriteLine(MovementCount);


            if (obj.Key == Key.Up) Gravity = Physics.GravityDirection.Haut;
            else if (obj.Key == Key.Left) Gravity = Physics.GravityDirection.Gauche;
            else if (obj.Key == Key.Down) Gravity = Physics.GravityDirection.Bas;
            else if (obj.Key == Key.Right) Gravity = Physics.GravityDirection.Droite;
        }


        public override string ToString()
        {
            return $"{typeof(KeyboardInput)}";
        }
    }
}
