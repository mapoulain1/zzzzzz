﻿using System.Collections.Generic;
using System.Linq;
using Urho.Audio;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Sounds
{
    public enum SoundType
    {
        Alarm,
        Bioup,
        Bup,
        Didadidadou,
        Jump,
        Notification,
        Sad,
    }

    public class SoundManager : SoundSource
    {
        private readonly List<(SoundType sound, string path)> sounds;

        public SoundManager()
        {
            GameManager.Current.Scene.CreateChild("SoundManager").AddComponent(this);
            Gain = 1;
            sounds = new List<(SoundType sound, string path)>()
            {
                (Sounds.SoundType.Alarm, Assets.Sounds.Alarm),
                (Sounds.SoundType.Bioup, Assets.Sounds.Bioup),
                (Sounds.SoundType.Bup, Assets.Sounds.Bup),
                (Sounds.SoundType.Didadidadou, Assets.Sounds.Didadidadou),
                (Sounds.SoundType.Jump, Assets.Sounds.Jump),
                (Sounds.SoundType.Notification, Assets.Sounds.Notification),
                (Sounds.SoundType.Sad, Assets.Sounds.Sad),
            };
        }

        public void Play(SoundType sound)
        {
            Play(Application.ResourceCache.GetSound(sounds.First(x => x.sound == sound).path));
        }

        public void GainCycle()
        {
            Gain = (((Gain * 100) + 10) % 110) / 100;
        }
    }
}
