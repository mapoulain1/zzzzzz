﻿using System.Collections.Generic;
using System.Linq;
using Urho.Audio;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Sounds
{
    public enum MusicType
    {
        Menu,
        Game1,
        Game2,
        Game3,
    }

    public class MusicManager : SoundSource
    {
        private readonly List<(MusicType music, string path)> musics;

        public MusicManager()
        {
            GameManager.Current.Scene.CreateChild("SoundManager").AddComponent(this);
            Gain = 0.8f;

            musics = new List<(MusicType music, string path)>()
            {
                (MusicType.Menu, Assets.Musics.Menu),
                (MusicType.Game1, Assets.Musics.Game1),
                (MusicType.Game2, Assets.Musics.Game2),
                (MusicType.Game3, Assets.Musics.Game3),
            };
        }

        public void Play(MusicType music)
        {
            Play(Application.ResourceCache.GetSound(musics.First(x => x.music == music).path));
        }

        public void GainCycle()
        {
            Gain = (((Gain * 100) + 10) % 110) / 100;
        }
    }
}
