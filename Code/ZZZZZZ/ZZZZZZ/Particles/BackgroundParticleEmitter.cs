﻿using System;
using System.Threading.Tasks;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Physics;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Particles
{
    public class BackgroundParticleEmitter : ParticleEmitter2D
    {
        private readonly Vector2 topLeft;
        private readonly Vector2 bottomRight;
        private readonly Vector3 middle;

        public BackgroundParticleEmitter()
        {
            GameManager.Current.Scene.CreateChild("BackgroundParticleEmitter").AddComponent(this);
            Effect = Application.Current.ResourceCache.GetParticleEffect2D(Assets.Particles.Background);
            topLeft = new Vector2(0, 0);
            bottomRight = new Vector2(7.68f, -4.80f);
            middle = new Vector3(7.68f, 4.80f, 0) / 2;
            Enabled = false;
            ProcessGravity();

        }


        public void Start()
        {
            Task.Run(() => MoveNode());
        }

        private void MoveNode()
        {
            try
            {
                Application.InvokeOnMain(() => Enabled = true);
                while (!IsDeleted)
                {
                    ProcessGravity();
                    Task.Delay(10).Wait();
                }
                Application.InvokeOnMain(() => Enabled = false);
            }
            catch (InvalidOperationException) { }

        }


        private void ProcessGravity()
        {
            switch (GameManager.Current.Scene.Gravity)
            {
                case GravityDirection.Bas:
                    Node.Position = GameManager.Current.Camera.Node.Position - middle + new Vector3(ToolBox.RandomFloat(topLeft.X, bottomRight.X), topLeft.Y + 4.80f, 0);
                    Effect.Angle = -90;
                    break;
                case GravityDirection.Haut:
                    Node.Position = GameManager.Current.Camera.Node.Position - middle + new Vector3(ToolBox.RandomFloat(topLeft.X, bottomRight.X), bottomRight.Y, 0);
                    Effect.Angle = 90;
                    break;
                case GravityDirection.Gauche:
                    Node.Position = GameManager.Current.Camera.Node.Position - middle + new Vector3(bottomRight.X, ToolBox.RandomFloat(topLeft.Y, bottomRight.Y) + 4.80f, 0);
                    Effect.Angle = 180;
                    break;
                case GravityDirection.Droite:
                    Node.Position = GameManager.Current.Camera.Node.Position - middle + new Vector3(topLeft.X, ToolBox.RandomFloat(topLeft.Y, bottomRight.Y) + 4.80f, 0);
                    Effect.Angle = 0;
                    break;
            }
        }

    }
}
