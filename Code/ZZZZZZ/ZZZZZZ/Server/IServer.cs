﻿using Urho;
using ZZZZZZ.Player;

namespace ZZZZZZ.Server
{
    public interface IServer
    {
        Vector2 RemotePosition { get; }
        (bool X, bool Y) RemoteFlipped { get; }
        LocalPlayerManager PlayerManager { get; set; }

        bool Connect(string ip, int port);
        void Start();
        void Disconnect();
        void RunSend(string ip, int port);
        void RunReceive(string ip, int port);

    }
}
