﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Urho;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Player;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Server
{
    public class ServerUDP : IServer
    {


        public Vector2 RemotePosition { get; private set; }
        public (bool X, bool Y) RemoteFlipped { get; private set; }
        private Socket TCPConnection { get; set; }
        public LocalPlayerManager PlayerManager { get; set; }

        private string serverIP;
        private int serverUDPPort;
        private int clientUDPPort;

        public ServerUDP()
        {
            TCPConnection = null;
        }

        public bool Connect(string ip, int port)
        {
            serverIP = ip;
            TCPConnection = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {

                var result = TCPConnection.BeginConnect(IPAddress.Parse(ip), port, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(5000, true);
                if (!success) return false;

                clientUDPPort = ToolBox.AvailableUDPPort();

                var bytes = Encoding.ASCII.GetBytes($"{clientUDPPort}\n");
                TCPConnection.Send(bytes);
                Console.WriteLine($"using local port {clientUDPPort}");

                byte[] receiveBytes = new byte[64];
                TCPConnection.Receive(receiveBytes);
                serverUDPPort = int.Parse(Encoding.ASCII.GetString(receiveBytes));
                Console.WriteLine($"using remote port {serverUDPPort}");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        public void Start()
        {
            RunSend(serverIP, serverUDPPort);
            RunReceive(serverIP, clientUDPPort);
        }

        public void Disconnect()
        {
            try
            {
                if (TCPConnection != null) TCPConnection.Close();
            }
            catch (Exception) { }
        }

        public void RunSend(string ip, int port)
        {
            Thread thread = new Thread(() => Send(ip, port));
            thread.Start();
        }
        public void RunReceive(string ip, int port)
        {
            Thread thread = new Thread(() => Receive(ip, port));
            thread.Start();
        }

        private void Send(string ip, int port)
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var remote = new IPEndPoint(IPAddress.Parse(ip), port);
            var character = (PlayerManager.Character as LocalCharacter);
            try
            {
                while (true)
                {
                    var bytes = Encoding.ASCII.GetBytes($"{ToolBox.GenerateQuerry(character)}\n");
                    var sent = socket.SendTo(bytes, remote);
                    Thread.Sleep(16);
                }
            }
            catch (ThreadInterruptedException) { }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                socket.Close();
            }


        }

        private void Receive(string ip, int port)
        {
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint remote = new IPEndPoint(IPAddress.Parse(ip), 0);
            var local = new IPEndPoint(IPAddress.Any, port);

            try
            {
                socket.Bind(local);
                byte[] bytes = new byte[256];
                while (true)
                {
                    socket.ReceiveFrom(bytes, ref remote);
                    var receive = Encoding.ASCII.GetString(bytes);
                    var decrypted = ToolBox.TryParseQuerry(receive);
                    if (decrypted != null)
                        (RemotePosition, RemoteFlipped) = decrypted.Value;
                }
            }
            catch (ThreadInterruptedException) { }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                socket.Close();
            }
        }

        ~ServerUDP()
        {
            Disconnect();
        }
    }
}
