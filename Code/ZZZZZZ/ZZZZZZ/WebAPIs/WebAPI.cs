﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Text;
using Urho;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Objects.Character;

namespace ZZZZZZ.WebAPIs
{
    public class WebAPI : IWebAPI
    {
        public string Code { get; set; }
        public bool Running { get; set; }
        public string Hook { get; }
        public HttpClient Client { get; }

        public WebAPI(string hook = "http://91.170.211.147:50000/")
        {
            Running = true;
            Hook = hook;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; };
            Client = new HttpClient(handler);
        }

        public bool CreateGame()
        {
            try
            {
                var post = Client.PostAsync($"{Hook}Game/CreateGame", null);
                post.Wait();
                var content = post.Result.Content.ReadAsStringAsync();
                content.Wait();
                Code = content.Result;
                return post.Result.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public int JoinGame(string code)
        {
            try
            {
                if (code == string.Empty)
                    code = Code;
                var post = Client.PostAsync($"{Hook}Game/JoinGame?code={code}", null);
                post.Wait();
                var content = post.Result.Content.ReadAsStringAsync();
                content.Wait();
                return int.Parse(content.Result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        public bool UpdateLocal(LocalCharacter player)
        {
            try
            {
                var post = Client.PutAsync($"{Hook}Game/SendPlayer?code={Code}&id={player.CharacterID}&x={FormatFloat(player.Node.Position2D.X)}&y={FormatFloat(player.Node.Position2D.Y)}&flipX={player.Flip.X}&flipY={player.Flip.Y}&rotation={(int)player.Node.Rotation2D}", null);
                post.Wait();
                var content = post.Result.Content.ReadAsStringAsync();
                content.Wait();
                return post.Result.StatusCode == HttpStatusCode.OK;
                }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool UpdateRemote(RemoteCharacter remote)
        {
            try
            {
                var get = Client.GetAsync($"{Hook}Game/GetPlayer?code={Code}&ownId={(remote.CharacterID + 1) % 2}");

                get.Wait();
                var content = get.Result.Content.ReadAsStringAsync();
                content.Wait();

                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(content.Result);
                if (values != null)
                {
                    //{"id":1,"x":0,"y":0,"flipX":false,"flipY":false,"online":false}
                    var x = float.Parse(values["x"], CultureInfo.InvariantCulture);
                    var y = float.Parse(values["y"], CultureInfo.InvariantCulture);
                    var flipX = bool.Parse(values["flipX"]);
                    var flipY = bool.Parse(values["flipY"]);
                    var online = bool.Parse(values["online"]);
                    var rotation = int.Parse(values["rotation"]);

                    remote.Move(new Vector2(x, y));
                    remote.Flip = (flipX, flipY);
                    remote.Node.Rotation2D = rotation;
                    remote.Online = online;
                }

                return values != null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool IsGameReady()
        {
            try
            {
                var get = Client.GetAsync($"{Hook}Game/IsGameReady?code={Code}");
                get.Wait();
                var content = get.Result.Content.ReadAsStringAsync();
                content.Wait();
                var result = content.Result;
                return bool.Parse(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private string FormatFloat(float value) => value.ToString("0.00", CultureInfo.InvariantCulture);

    }
}
