﻿using System.Net.Http;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Objects.Character;


namespace ZZZZZZ.WebAPIs
{
    public interface IWebAPI
    {

        string Code { get; set; }
        bool Running { get; set; }
        HttpClient Client { get; }

        bool CreateGame();
        int JoinGame(string code = "");
        bool UpdateLocal(LocalCharacter player);
        bool UpdateRemote(RemoteCharacter remote);
        bool IsGameReady();



    }
}
