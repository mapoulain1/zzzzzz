﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Urho.Network;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Inputs;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Player;
using ZZZZZZ.Server;
using ZZZZZZ.Utils;
using ZZZZZZ.WebAPIs;

namespace ZZZZZZ
{
    public partial class GameManager
    {

        public void SpawnPlayer()
        {
            LocalPlayerManager = new LocalPlayerManager(new LocalCharacter(), ToolBox.AdaptedInput(), LevelManager);
            //LocalPlayerManager = new LocalPlayerManager(new LocalCharacter(), new JoystickInput(), LevelManager);
        }

        public void DispawnPlayer()
        {
            LocalPlayerManager.Dispawn();
            LocalPlayerManager = null;
        }

        public void ClearGame()
        {
            if (LocalPlayerManager != null)
            {
                LocalPlayerManager.Dispawn();
                LocalPlayerManager = null;
            }
            if(LevelManager != null)
            {
                LevelManager.Dispose();
            }
        }

  
        public void ConnectToAPI(string gamecode = "")
        {
            Task.Run(() =>
            {
                //API = new WebAPI("https://localhost:5000/");
                API = new WebAPI();
                if (string.IsNullOrEmpty(gamecode))
                {
                    API.CreateGame();
                    System.Console.WriteLine($"Created game with ID {API.Code}");
                    Navigation.Toast.Show($"Created game with ID {API.Code}");
                }
                else
                {
                    API.Code = gamecode;
                }

                var joinedID = API.JoinGame();
                if (joinedID != -1)
                {
                    int counter = 0;
                    while (counter < 600 && !API.IsGameReady())
                    {
                        System.Console.WriteLine($"Game is not ready, try {counter}");
                        Task.Delay(1000).Wait();
                        counter++;
                    }

                    InvokeOnMain(() =>
                    {
                        LevelManager.Load(Assets.Levels.World0Start);
                        Navigation.Load(Assets.UI.Menus.Ingame);
                        LocalPlayerManager = new LocalPlayerManager(new LocalCharacter(joinedID), ToolBox.AdaptedInput(), LevelManager);
                        RemotePlayerManager = new RemotePlayerManager(new RemoteCharacter((joinedID + 1) % 2), LevelManager);
                    });
                    Task.Delay(50).Wait();
                    InvokeOnMain(() =>
                    {
                        Task.Run(() =>
                        {
                            while (API.Running)
                            {
                                DateTime before = DateTime.Now;
                                API.UpdateLocal(LocalPlayerManager.Character as LocalCharacter);
                                DateTime after = DateTime.Now;
                                TimeSpan delta = after - before;
                                var waiting = Math.Max(16 - delta.TotalMilliseconds, 0);
                                Task.Delay((int)waiting).Wait();
                            }
                        });

                        Task.Run(() =>
                        {
                            while (API.Running)
                            {
                                DateTime before = DateTime.Now;
                                API.UpdateRemote(RemotePlayerManager.Character as RemoteCharacter);
                                DateTime after = DateTime.Now;
                                TimeSpan delta = after - before;
                                var waiting = Math.Max(16 - delta.TotalMilliseconds, 0);
                                (RemotePlayerManager.Character as RemoteCharacter).ServerDelta = (float)delta.TotalSeconds;
                                Task.Delay((int)waiting).Wait();
                                
                            }
                        });
                    });
                }

            });
        }

    }
}
