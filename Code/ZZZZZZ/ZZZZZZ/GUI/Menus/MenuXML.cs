﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Urho;
using ZZZZZZ.GUI.Buttons;
using ZZZZZZ.GUI.Images;
using ZZZZZZ.GUI.Inputs;
using ZZZZZZ.GUI.Navigations;
using ZZZZZZ.GUI.Titles;

namespace ZZZZZZ.GUI.Menus
{
    public class MenuXML : Menu
    {
        private XDocument XDocument { get; set; }
        public override void Load(string filename)
        {
            try
            {
                var xmlFile = Application.Current.ResourceCache.GetFile(filename);
                var bytes = new byte[1024];
                xmlFile.Read(bytes, 1024);
                var content = Encoding.UTF8.GetString(bytes).Replace('\0', ' ');

                XDocument = XDocument.Load(new StringReader(content));
                var menu = XDocument.Root;
                LoadButtons(menu);
                LoadTitles(menu);
                LoadImages(menu);
                LoadTextInput(menu);
                LoadOnCreate(menu);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void LoadOnCreate(XElement menu)
        {
            var onCreateText = menu.Attribute("onCreate");
            if (onCreateText != null)
            {
                var onClick = Navigation.Current.GetType().GetMethod(onCreateText.Value);
                onClick?.Invoke(GameManager.Current.Navigation, null);
            }

        }

        private void LoadImages(XElement menu)
        {
            foreach (var image in menu.Elements("image").ToList())
            {
                var id = image.Attribute("id") == null ? string.Empty : image.Attribute("id").Value;
                var src = image.Attribute("src").Value;
                var scale = float.Parse(image.Attribute("scale").Value, CultureInfo.InvariantCulture);
                var x = float.Parse(image.Attribute("x").Value, CultureInfo.InvariantCulture);
                var y = float.Parse(image.Attribute("y").Value, CultureInfo.InvariantCulture);
                var onClick = Navigation.Current.GetType().GetMethod(image.Attribute("onClick") == null ? string.Empty : image.Attribute("onClick").Value);
                bool.TryParse(image.Attribute("animated") == null ? "true" : image.Attribute("animated").Value, out bool doAnimation);
                Elements.Add(new ImageCustom(id, x, y, scale, src, onClick, doAnimation));
            }
        }

        private void LoadTextInput(XElement menu)
        {
            foreach (var textInput in menu.Elements("input").ToList())
            {
                var id = textInput.Attribute("id") == null ? string.Empty : textInput.Attribute("id").Value;
                var text = textInput.Attribute("text").Value;
                var scale = float.Parse(textInput.Attribute("scale").Value, CultureInfo.InvariantCulture);
                var x = float.Parse(textInput.Attribute("x").Value, CultureInfo.InvariantCulture);
                var y = float.Parse(textInput.Attribute("y").Value, CultureInfo.InvariantCulture);
                Elements.Add(new TextInput(id, text, x, y, scale));
            }
        }


        private void LoadTitles(XElement menu)
        {
            foreach (var title in menu.Elements("title").ToList())
            {
                var id = title.Attribute("id") == null ? string.Empty : title.Attribute("id").Value;
                var text = title.Attribute("text").Value;
                var scale = float.Parse(title.Attribute("scale").Value, CultureInfo.InvariantCulture);
                var x = float.Parse(title.Attribute("x").Value, CultureInfo.InvariantCulture);
                var y = float.Parse(title.Attribute("y").Value, CultureInfo.InvariantCulture);
                var animated = float.Parse(title.Attribute("animated") == null ? "0" : title.Attribute("animated").Value);
                Elements.Add(new TitleCustom(id, text, x, y, scale, animated));
            }
        }

        private void LoadButtons(XElement menu)
        {
            foreach (var button in menu.Elements("button").ToList())
            {
                var id = button.Attribute("id") == null ? string.Empty : button.Attribute("id").Value;
                var text = button.Attribute("text").Value;
                var scale = float.Parse(button.Attribute("scale").Value, CultureInfo.InvariantCulture);
                var x = float.Parse(button.Attribute("x").Value, CultureInfo.InvariantCulture);
                var y = float.Parse(button.Attribute("y").Value, CultureInfo.InvariantCulture);
                var onClick = Navigation.Current.GetType().GetMethod(button.Attribute("onClick").Value);
                Elements.Add(new ButtonCustom(id, text, x, y, scale, onClick));
            }
        }


    }
}
