﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Menus
{
    public abstract class Menu
    {
        public event Action<UIElement> Clicked
        {
            add => _clicked += value;
            remove => _clicked -= value;
        }
        public IEnumerable<Component> Children { get => new ReadOnlyCollection<Component>(Elements); }

        public List<Component> Elements { get; }
        protected BorderImage Background { get; }

        protected Action<UIElement> _clicked;

        public Menu()
        {
            Elements = new List<Component>();
            Background = new BorderImage
            {
                Opacity = 0.2f
            };
            Application.Current.UI.UIMouseClick += UI_UIMouseClick;
        }

        private void UI_UIMouseClick(UIMouseClickEventArgs obj)
        {
            try
            {
                var element = Elements.First(x => !x.IsDeleted && ToolBox.IsPosInRect(obj.X, obj.Y, x.CombinedScreenRect));
                if (element != null)
                    _clicked?.Invoke(element);
            }
            catch (InvalidOperationException) { }
        }
        public void Hide() => Elements.ForEach(x => x.Visible = false);
        public void Show() => Elements.ForEach(x => x.Visible = true);
        public void Destroy() => Elements.ForEach(x => Application.Current.UI.Root.RemoveChild(x));


        public abstract void Load(string filename);
    }
}
