﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI
{
    public abstract class Component : BorderImage
    {
        public string ID { get; }
        public Text Text { get; set; }
        public new bool Enabled
        {
            get => Opacity < 0.4f;
            set => Opacity = value ? 0.9f : 0.3f;
        }

        protected float _x;
        protected float _y;
        protected float _scale;

     

        public Component(string id, float x, float y, float scale)
        {
            (_x, _y) = (x, y);
            _scale = scale;
            ID = id;
            Application.Current.UI.UIMouseClick += UI_UIMouseClick;
            Application.Current.UI.Root.AddChild(this);
        }

        protected virtual void UI_UIMouseClick(UIMouseClickEventArgs obj) { }

        protected void Update(bool doAnimation = true)
        {
            Size = ToolBox.PercentToPixelButton(_scale / 100);
            var position = ToolBox.PercentToPixel((_x) / 100, (_y) / 100);
            Position = new IntVector2(position.X - Size.X / 2, position.Y - Size.Y / 2);
            if (Text != null)
                Text.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), ToolBox.FontSize(Text.Value, Size.X));

            if (doAnimation)
            {
                Task.Run(() => DoAnimation());
                Task.Delay(1100).ContinueWith(t => SetVisible());
            }
        }

        protected void SetupText(string text)
        {
            Text = new Text
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Value = text,
            };
            Text.SetColor(Color.White);
            Text.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), ToolBox.FontSize(text, Size.X));
            AddChild(Text);
        }

        [HandleProcessCorruptedStateExceptions]
        protected void DoAnimation()
        {
            try
            {
                if (IsDeleted) return;
                Opacity = 0;
                if (Text != null) Text.Opacity = 0;
                int i = 0;
                while (!IsDeleted && i < 60)
                {
                    Opacity += 0.017f;
                    if (Text != null) Text.Opacity += 0.017f;
                    Thread.Sleep(16);
                    i++;
                }
            }
            catch (AccessViolationException) { }
            catch (InvalidOperationException) { }
        }
        protected void DoAnimationBackward()
        {
            try
            {
                if (IsDeleted) return;
                Opacity = 1;
                if (Text != null) Text.Opacity = 1;
                int i = 60;
                while (i > 0 && !IsDeleted)
                {
                    Opacity += 0.017f;
                    if (Text != null) Text.Opacity -= 0.017f;
                    Thread.Sleep(16);
                    i--;
                }
                Opacity = 0;
                if (Text != null) Text.Opacity = 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected void SetVisible()
        {
            try
            {
                Opacity = 1;
                if (Text != null) Text.Opacity = 1;
            }
            catch (InvalidOperationException) { }
        }


    }
}
