﻿using System.Reflection;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Images
{
    public class ImageCustom : Component
    {
        protected MethodInfo OnClick { get; }

          public ImageCustom(string text, float x, float y, float scale, string src, MethodInfo onClick, bool doAnimation = true) : base(text, x, y, scale)
        {
            Texture = Application.Current.ResourceCache.GetTexture2D($"{Assets.UI.Menus.Images.IconPath}{src}");
            Texture.FilterMode = TextureFilterMode.NearestAnisotropic;
            OnClick = onClick;
            Update(doAnimation);
        }

        protected override void UI_UIMouseClick(UIMouseClickEventArgs obj)
        {
            base.UI_UIMouseClick(obj);
            if (IsDeleted) return;
            if (ToolBox.IsPosInRect(obj.X, obj.Y, new IntRect(Position.X, Position.Y, Position.X + Size.X, Position.Y + Size.Y)))
            {
                OnClick?.Invoke(GameManager.Current.Navigation, null);
            }
        }

    }
}
