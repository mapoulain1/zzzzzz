﻿using System;
using System.Diagnostics;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Inputs
{
    public class OnScreenJoystick : BorderImage
    {
        public new bool Visible
        {
            get { return base.Visible; }
            set
            {
                base.Visible = value;
                innerJoystick.Visible = value;
            }
        }

        public bool LockedX { get; set; }
        public bool LockedY { get; set; }

        public float X { get => (innerJoystick.Position.X - originalPosition.X) / (float)radius; }
        public float Y { get => (innerJoystick.Position.Y - originalPosition.Y) / (float)radius; }


        private readonly BorderImage innerJoystick;
        private bool clicked;
        private IntVector2 originalPosition;
        private readonly int radius;
        private int currentTouchID;
        public OnScreenJoystick(IntVector2 position)
        {
            currentTouchID = -1;
            clicked = false;
            radius = 20;
            LockedX = false;
            LockedY = false;

            innerJoystick = new BorderImage
            {
                Texture = Application.Current.ResourceCache.GetTexture2D(Assets.UI.JoystickInner),
                Position = position,
                Size = IntVector2.One * Application.Current.Graphics.Width / 8,
            };

            {
                Texture = Application.Current.ResourceCache.GetTexture2D(Assets.UI.JoystickOuter);
                Position = position;
                Size = IntVector2.One * Application.Current.Graphics.Width / 8;
            }
            originalPosition = innerJoystick.Position;


            Application.Current.UI.Root.AddChild(this);
            Application.Current.UI.Root.AddChild(innerJoystick);
            Application.Current.Input.TouchBegin += Input_TouchBegin;
            Application.Current.Input.TouchMove += Input_TouchMove;
            Application.Current.Input.TouchEnd += Input_TouchEnd;
        }

        private void Input_TouchMove(TouchMoveEventArgs obj)
        {
            if (clicked && obj.TouchID == currentTouchID)
            {
                var distance = Math.Sqrt((innerJoystick.Position.X + obj.DX - originalPosition.X) * (innerJoystick.Position.X + obj.DX - originalPosition.X) + (innerJoystick.Position.Y + obj.DY - originalPosition.Y) * (innerJoystick.Position.Y + obj.DY - originalPosition.Y));
                var x = innerJoystick.Position.X;
                var y = innerJoystick.Position.Y;
                if (distance > radius)
                {
                    if (!LockedX) x += (int)((innerJoystick.Position.X + obj.DX - innerJoystick.Position.X) * (radius / distance));
                    if (!LockedY) y += (int)((innerJoystick.Position.Y + obj.DY - innerJoystick.Position.Y) * (radius / distance));
                }
                else
                {
                    if (!LockedX) x += obj.DX;
                    if (!LockedY) y += obj.DY;
                }
                innerJoystick.Position = new IntVector2(x, y);
            }
        }

        private void Input_TouchBegin(TouchBeginEventArgs obj)
        {
            if (innerJoystick.IsDeleted) return;
            if (currentTouchID == obj.TouchID || currentTouchID == -1)
            {
                clicked = ToolBox.IsPosInRect(obj.X, obj.Y, new IntRect(innerJoystick.Position.X, innerJoystick.Position.Y, innerJoystick.Position.X + innerJoystick.Size.X, innerJoystick.Position.Y + innerJoystick.Size.Y));
                currentTouchID = clicked ? obj.TouchID : currentTouchID;
            }
        }

        private void Input_TouchEnd(TouchEndEventArgs obj)
        {
            if (obj.TouchID == currentTouchID)
            {
                clicked = false;
                innerJoystick.Position = originalPosition;
                currentTouchID = -1;
            }
        }
    }
}
