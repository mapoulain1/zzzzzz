﻿using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Inputs
{
    public class TextInput : Component
    {
        public string Value { get => realText; }
        private LineEdit lineEdit;
        private string realText;
        private int position;

        public TextInput(string id, string text, float x, float y, float scale) : base(id, x, y, scale)
        {
            position = 0;
            realText = text;
            Update();
            lineEdit = new LineEdit
            {
                Text = text,
                Editable = true,
                Width = this.Width,
                Height = this.Height,
            };
            lineEdit.SetColor(Color.Transparent);
            Texture = Application.Current.ResourceCache.GetTexture2D(Assets.UI.Menus.Images.ButtonBackground);
            Texture.FilterMode = TextureFilterMode.NearestAnisotropic;

            lineEdit.TextChanged += LineEdit_TextChanged;
            AddChild(lineEdit);
            SetupText(text);
        }

        private void LineEdit_TextChanged(TextChangedEventArgs obj)
        {
            if (Text != null)
            {
                realText = obj.Text;
                if (obj.Text.Length > 15)
                {
                    if (realText.Length < obj.Text.Length) position++;
                    if (realText.Length > obj.Text.Length) position--;
                    Text.Value = realText.Substring(position);
                }
                else
                {
                    Text.Value = obj.Text;
                    Text.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), ToolBox.FontSize(Text.Value, Size.X));
                }



            }
        }
    }
}
