﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Titles
{
    public class TitleCustom : Component
    {
        public TitleCustom(string id, string text, float x, float y, float scale, float animatedTime) : base(id, x, y, scale)
        {
            SetColor(Color.Transparent);
            Update(false);
            SetupText(text);
            Text.SetFontSize(ToolBox.FontSize(text, Size.X, 10));

            Task.Run(() => IntroAnimation(animatedTime));
        }



        private void IntroAnimation(float millis)
        {
            try
            {
                int i = 0;
                float to = millis / 16;
                while (i < to && !IsDeleted)
                {
                    Text.Opacity = (i / to) * (i / to);
                    Thread.Sleep(16);
                    i++;
                }
            }
            catch (Exception) { }
        }
    }
}
