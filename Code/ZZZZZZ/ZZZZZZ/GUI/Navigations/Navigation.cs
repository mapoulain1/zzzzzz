﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ZZZZZZ.GUI.Menus;
using ZZZZZZ.GUI.Toasts;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {
        public Menu Menu { get; private set; }
        public Toast Toast { get; }

        public static Navigation Current
        {
            get { if (_current == null) _current = new Navigation(); return _current; }
        }
        private static Navigation _current;

        private Navigation()
        {
            Toast = new Toast();
        }



        public void Load(string menu)
        {
            if (Menu != null) Menu.Destroy();
            Menu = new MenuXML();
            Menu.Load(menu);
            if (menu == Assets.UI.Menus.Intro)
                Task.Delay(700).ContinueWith(t => { GameManager.InvokeOnMain(() => GameManager.Current.Navigation.Load(Assets.UI.Menus.Main)); });
        }

        public void ButtonSound()
        {
            GameManager.Current.SoundManager.Play(Sounds.SoundType.Bup);
        }

        public Component FindById(string id)
        {
            try
            {
                return Menu.Children.First(x => x.ID == id);
            }
            catch (Exception) { return null; }

        }

    }
}
