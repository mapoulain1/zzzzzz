﻿using ZZZZZZ.GUI.Buttons;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {

        public void OptionOnCreate()
        {
            OptionUpdateButtons();
        }

        public void OptionMusicClicked()
        {
            ButtonSound();
            GameManager.Current.MusicManager.GainCycle();
            OptionUpdateButtons();
        }
        public void OptionSoundClicked()
        {
            ButtonSound();
            GameManager.Current.SoundManager.GainCycle();
            OptionUpdateButtons();
        }

        private void OptionUpdateButtons()
        {
            var music = FindById("MusicButton") as ButtonCustom;
            if (music != null) music.Text.Value = MusicText;
            var sound = FindById("SoundButton") as ButtonCustom;
            if (sound != null) sound.Text.Value = SoundText;
        }

        public void OptionBackClicked()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Main);
        }

        private string MusicText { get => $"MUSIC {(int)(GameManager.Current.MusicManager.Gain * 100)}"; }
        private string SoundText { get => $"SOUND {(int)(GameManager.Current.SoundManager.Gain * 100)}"; }

    }
}
