﻿using System;
using System.Threading.Tasks;
using ZZZZZZ.GUI.Inputs;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {

        public void MultiplayerBackClicked()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Main);
        }
        public void MultiplayerJoinClicked()
        {
            ButtonSound();

            try
            {
                var code = (FindById("ServerCode") as TextInput).Value;
                GameManager.Current.ConnectToAPI(code);
            }
            catch (ArgumentException e)
            {
                GameManager.Current.Navigation.Toast.Show(e.Message);
            }
        }

        public void MultiplayerCreateClicked()
        {
            ButtonSound();
            try
            {
                GameManager.Current.ConnectToAPI();
            }
            catch (ArgumentException e)
            {
                GameManager.Current.Navigation.Toast.Show(e.Message);
            }
        }

    }
}
