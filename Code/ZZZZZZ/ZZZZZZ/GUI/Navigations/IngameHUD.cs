﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Gui;
using ZZZZZZ.GUI.Buttons;
using ZZZZZZ.GUI.Images;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {
        private ImageCustom opacityBackground;
        private ButtonCustom backButton;

        public void IngameHUDOnCreate()
        {
            backButton = FindById("backButton") as ButtonCustom;
            if (backButton != null)
                backButton.Visible = false;
            opacityBackground = FindById("opacityBackground") as ImageCustom;
            if (opacityBackground != null)
            {
                opacityBackground.Opacity = 0.9f;
                opacityBackground.Visible = false;
                opacityBackground.Position = IntVector2.Zero;
                opacityBackground.Size = Application.Current.Graphics.Size;
            }
        }

        public void IngamePauseClicked()
        {
            ButtonSound();
            GameManager.Current.Scene.UpdateEnabled = !GameManager.Current.Scene.UpdateEnabled;
            var running = GameManager.Current.Scene.UpdateEnabled;
            if (opacityBackground != null)
            {
                opacityBackground.Opacity = 0.9f;
                opacityBackground.Visible = !running;
            }
            if (backButton != null)
                backButton.Visible = !running;
            GameManager.Current.LocalPlayerManager.Input.Visible = running;
        }

        public void IngameBackClicked()
        {
            GameManager.Current.Scene.UpdateEnabled = true;
            GameManager.Current.ClearGame();
            Load(Assets.UI.Menus.Main);
            Toast.Clear();
        }
    }
}
