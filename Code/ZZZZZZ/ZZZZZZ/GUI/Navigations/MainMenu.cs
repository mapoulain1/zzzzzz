﻿using ZZZZZZ.Utils;


namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {
        public void MainMultiplayerClicked()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Multiplayer);
        }

        public void MainSingleplayerClicked()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Singleplayer);
        }

        public void MainOptionClicked()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Option);
        }
    }
}
