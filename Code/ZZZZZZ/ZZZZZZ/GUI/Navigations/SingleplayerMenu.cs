﻿using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Navigations
{
    public partial class Navigation
    {
        public void SingleplayerWorld0()
        {
            ButtonSound();
            GameManager.Current.LevelManager.Load(Assets.Levels.World0Start);
            Load(Assets.UI.Menus.Ingame);
            GameManager.Current.SpawnPlayer();
            GameManager.Current.Navigation.Toast.Show("Welcome to ZZZZZZ");
            GameManager.Current.Navigation.Toast.Show("Let's begin slowly");
        }

        public void SingleplayerWorld1()
        {
            ButtonSound();
            GameManager.Current.LevelManager.Load(Assets.Levels.World1Start);
            Load(Assets.UI.Menus.Ingame);
            GameManager.Current.SpawnPlayer();
            GameManager.Current.Navigation.Toast.Show("Come and die");
            GameManager.Current.Navigation.Toast.Show("It won't hurt");
            GameManager.Current.Navigation.Toast.Show("yet");
        }

        public void SingleplayerWorld2()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Ingame);
        }

        public void SingleplayerWorld3()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Ingame);
        }
        public void SingleplayerWorld4()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Ingame);
        }

        public void SingleplayerWorld5()
        {
            ButtonSound();
            Load(Assets.UI.Menus.Ingame);
        }

    }
}
