﻿using System.Threading.Tasks;
using Urho;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Player;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI
{
    public class ExtraFunction
    {
        private bool showLocalPosition { get; set; }
        public ExtraFunction()
        {
            Application.Current.Input.KeyDown += Input_KeyDown;
        }

        private void Input_KeyDown(KeyDownEventArgs obj)
        {
            switch (obj.Key)
            {
                case Key.L:
                    System.Console.WriteLine((GameManager.Current.LocalPlayerManager.Character as LocalCharacter).Body.Node.Position.ToString());
                    break;
                case Key.F3:
                    AppConfig.Current.DrawDebug = !AppConfig.Current.DrawDebug;
                    GameManager.Current.Camera.Zoom = AppConfig.Current.DrawDebug ? 1 : 4.2f;
                    break;
                case Key.F10:
                    Task.Run(() => GameManager.Current.Exit());
                    break;
                case Key.KP_2:
                    GameManager.Current.Scene.Gravity = Physics.GravityDirection.Bas;
                    break;
                case Key.KP_8:
                    GameManager.Current.Scene.Gravity = Physics.GravityDirection.Haut;
                    break;
                case Key.KP_4:
                    GameManager.Current.Scene.Gravity = Physics.GravityDirection.Gauche;
                    break;
                case Key.KP_6:
                    GameManager.Current.Scene.Gravity = Physics.GravityDirection.Droite;
                    break;
            }

        }
    }
}
