﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Toasts
{
    public class Toast : Component
    {
        private readonly Queue<string> messages;
        public Toast(string text = "", float x = 50, float y = 80, float scale = 20) : base(string.Empty, x, y, scale)
        {
            BlendMode = BlendMode.InvDestAlpha;
            Update(false);
            SetupText(text);
            Visible = false;
            messages = new Queue<string>();
        }

        public void Clear()
        {
            messages.Clear();
        }

        public void Show(string text, int time = 5000)
        {
            if (Visible)
            {
                messages.Enqueue(text);
                return;
            }
            Application.InvokeOnMain(() =>
            {
                Text.Value = text;
                Text.SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), MathHelper.Clamp(ToolBox.FontSize(text, Size.X), 20, 60));
                Visible = true;
            });

            Task.Run(() => DoAnimation());
            Task.Delay(time).ContinueWith(t => DoAnimationBackward());
            Task.Delay(time + 1000).ContinueWith(t => { Application.InvokeOnMain(() => Visible = false); });
            Task.Delay(time + 1100).ContinueWith(t =>
            {
                Application.InvokeOnMain(() =>
                {
                    if (messages.Count > 0)
                    {
                        Show(messages.Dequeue());
                    }
                });
            });
        }
    }
}
