﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI.Buttons
{
    public class ButtonCustom : Component
    {
        protected MethodInfo OnClick { get; }


        public ButtonCustom(string id, string text, float x, float y, float scale, MethodInfo onClick) : base(id, x, y, scale)
        {

            Texture = Application.Current.ResourceCache.GetTexture2D(Assets.UI.Menus.Images.ButtonBackground);
            Texture.FilterMode = TextureFilterMode.NearestAnisotropic;
            OnClick = onClick;
            Update();
            SetupText(text);
        }

        protected override void UI_UIMouseClick(UIMouseClickEventArgs obj)
        {
            base.UI_UIMouseClick(obj);
            if (IsDeleted) return;
            if (!Visible) return;

            if (ToolBox.IsPosInRect(obj.X, obj.Y, new IntRect(Position.X, Position.Y, Position.X + Size.X, Position.Y + Size.Y)))
            {
                try
                {
                    OnClick?.Invoke(GameManager.Current.Navigation, null);
                }
                catch (TargetInvocationException e)
                {
                    Console.WriteLine(e);
                }
                Task.Run(() => ClickedAnimation());
                }
            }
            private void ClickedAnimation()
            {
                try
                {
                    int i = 0;
                    while (i < 60 && !IsDeleted)
                    {
                        Opacity = (float)ToolBox.BounceAnimation(i);
                        Thread.Sleep(6);
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

        }
    }
