﻿using ZZZZZZ.Entities.Character;
using ZZZZZZ.Worlds;

namespace ZZZZZZ.Player
{
    public interface IPlayerManager
    {
        ICharacter Character { get; }
        LevelManager LevelManager { get; }
        void Dispawn();

    }
}
