﻿using Urho;
using Urho.Actions;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Inputs;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Worlds;

namespace ZZZZZZ.Player
{
    public class LocalPlayerManager : IPlayerManager
    {
        public ICharacter Character { get; private set; }
        public AbstractInput Input { get; private set; }
        public LevelManager LevelManager { get; }


        public LocalPlayerManager(LocalCharacter character, AbstractInput input, LevelManager levelManager)
        {
            LevelManager = levelManager;
            Character = character;
            Input = input;
            if (LevelManager.Current.Spawn != null)
                Character.Move(LevelManager.Current.Spawn.Value);

            Application.Current.Update += Current_Update;
            LevelManager.LevelChanged += LevelManager_LevelChanged;

        }

        private void LevelManager_LevelChanged(LevelChangedEventArgs obj)
        {
            var node = (Character as LocalCharacter).Node;
            node.Scale = Vector3.Zero;
            Character.Move(obj.SpawnLocation);
            node.RunActionsAsync(new ScaleTo(0.3f, 10));

        }

        private void Current_Update(UpdateEventArgs obj)
        {
            if (Character == null || (Character as LocalCharacter).Deleted) return;
            Character.Move(Input.Movement);
            if ((Character as LocalCharacter).IsGrounded) 
                GameManager.Current.Scene.Gravity = Input.Gravity;
            else
                Input.Gravity = GameManager.Current.Scene.Gravity;
        }

        public void Dispawn()
        {
            (Character as LocalCharacter).Delete();
        }
    }
}
