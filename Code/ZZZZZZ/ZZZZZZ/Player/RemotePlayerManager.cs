﻿using Urho;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Server;
using ZZZZZZ.Worlds;

namespace ZZZZZZ.Player
{
    public class RemotePlayerManager : IPlayerManager
    {
        public int CharacterID { get; set; }
        public ICharacter Character { get; private set; }
        public LevelManager LevelManager { get; private set; }
        public IServer Server { get; }

        public RemotePlayerManager(ICharacter character, LevelManager levelManager)
        {
            CharacterID = -1;
            LevelManager = levelManager;
            Character = character;
        }

      
        public void Dispawn()
        {
            (Character as RemoteCharacter).Dispose();
        }
    }
}
