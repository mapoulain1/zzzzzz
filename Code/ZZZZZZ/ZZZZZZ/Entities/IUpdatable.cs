﻿using Urho;

namespace ZZZZZZ.Entities
{
    public interface IUpdatable
    {
        void Update(UpdateEventArgs obj);

    }
}
