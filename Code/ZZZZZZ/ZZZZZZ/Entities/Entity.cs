﻿using System;
using Urho;
using Urho.Urho2D;

namespace ZZZZZZ.Entities
{
    public abstract class Entity : Component, IUpdatable
    {

        public StaticSprite2D Sprite { get; protected set; }
        public RigidBody2D Body { get; protected set; }
        public CollisionShape2D Shape { get; protected set; }
        public bool Deleted { get; protected set; } = false;

        public event Action<Entity> EntityWasDeleted
        {
            add => _entityWasDeleted += value;
            remove => _entityWasDeleted -= value;
        }
        protected Action<Entity> _entityWasDeleted;




        public Entity(bool wrapping = true, bool addToScene = true) : base()
        {

            if (addToScene)
            {
                GameManager.Current.Scene.CreateChild("Entity").AddComponent(this);

                Sprite = Node.CreateComponent<StaticSprite2D>();
                Body = Node.CreateComponent<RigidBody2D>();
                Body.BodyType = BodyType2D.Dynamic;

                Node.Position = Vector3.Zero;
            }

            ReceiveSceneUpdates = true;
            Application.Current.Update += Update;
        }




        public virtual void Update(UpdateEventArgs obj)
        {
            if (Deleted) return;
            base.OnUpdate(obj.TimeStep);
        }









        public virtual void Delete()
        {
            try
            {
                Application.Current.Update -= Update;
                GameManager.Current.Scene.RemoveChild(Node);
                Deleted = true;
                _entityWasDeleted?.Invoke(this);
            }
            catch (NullReferenceException e) { Console.WriteLine(e); }
            catch (InvalidOperationException e) { Console.WriteLine(e); }
        }

    }
}
