﻿using System.Collections.Generic;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Entities.Character
{
    public class RemoteCharacter : Entity, ICharacter
    {
        public int CharacterID { get; set; }
        public bool Online { get; set; }
        public float ServerDelta { get; set; }

        public (bool X, bool Y) Flip
        {
            get => (Sprite.FlipX, Sprite.FlipY);
            set => (Sprite.FlipX, Sprite.FlipY) = (value.X, value.Y);
        }

        private List<Sprite2D> Sprites;
        private float internalTimer;

        public RemoteCharacter(int id = -1)
        {
            ServerDelta = 16 / 1000;
            CharacterID = id;
            Online = false;
            Node.Scale *= 2;
            Sprites = new List<Sprite2D>
            {
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.CharacterRemote_1),
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.CharacterRemote_2)
            };
            foreach (var s in Sprites)
                s.Texture.FilterMode = TextureFilterMode.Nearest;

            Sprite.Sprite = Sprites[0];
            Body.BodyType = BodyType2D.Static;

        }
        public void Move(Movement movement) { }

        public void Move(Vector2 position)
        {
            Node.RunActionsAsync(new MoveTo(ServerDelta, new Vector3(position)));
        }
        public override void Update(UpdateEventArgs obj)
        {
            internalTimer = (internalTimer + obj.TimeStep) % 1000;
            Sprite.Sprite = Sprites[((int)(internalTimer * 2)) % Sprites.Count];
            if (Body.LinearVelocity.Length > 10f)
            {
                Body.SetLinearVelocity(Body.LinearVelocity * 0.90f);
            }
        }

    }
}
