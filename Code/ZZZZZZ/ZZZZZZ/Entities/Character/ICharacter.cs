﻿using Urho;

namespace ZZZZZZ.Entities.Character
{
    public interface ICharacter
    {
        int CharacterID { get; set; }
        (bool X, bool Y) Flip { get; set; }
        void Move(Movement movement);
        void Move(Vector2 position);

    }
}
