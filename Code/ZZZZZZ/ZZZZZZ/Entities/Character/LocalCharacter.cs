﻿using System.Collections.Generic;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Entities;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Physics;
using ZZZZZZ.Physics.CollisionBoxs;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Objects.Character
{
    public class LocalCharacter : Entity, ICharacter
    {
        public int CharacterID { get; set; }
        public float Speed { get; set; }
        public Checkpoint LastCheckpoint { get; set; }
        public bool IsGrounded { get; set; }
        public (bool X, bool Y) Flip
        {
            get => (Sprite.FlipX, Sprite.FlipY);
            set => (Sprite.FlipX, Sprite.FlipY) = (value.X, value.Y);
        }

        private readonly List<Sprite2D> Sprites;
        private float internalTimer;

        public LocalCharacter(int id = -1)
        {
            LastCheckpoint = null;
            CharacterID = id;
            Node.Scale *= 2;
            Node.SetPosition2D(new Vector2(1, 1));
            Speed = 5f;
            Shape = Node.CreateComponent<CollisionCircle2D>();
            (Shape as CollisionCircle2D).Radius = 0.075f;


            Body.UseFixtureMass = false;
            Body.Inertia = 5;
            Body.FixedRotation = true;

            internalTimer = 0f;

            Sprites = new List<Sprite2D>
            {
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.CharacterLocal_1),
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.CharacterLocal_2)
            };
            foreach (var s in Sprites)
                s.Texture.FilterMode = TextureFilterMode.Nearest;

            Sprite.Sprite = Sprites[0];
        }

        public void Move(Movement movement)
        {


            var vect = new Vector2(0, 0);
            var gravity = GameManager.Current.Scene.Gravity;
            /*
             * calculc du contact au sol
             *si l'objet est en contact, tester si c'est le bon sens si c'est le bon true sinon false
             * si la gravite a ete changee effectivement, alors false
             */

            switch (movement)
            {
                case Movement.Left:
                    vect = -Vector2.UnitX;
                    Sprite.FlipX = true;
                    break;
                case Movement.Right:
                    vect = Vector2.UnitX;
                    Sprite.FlipX = false;
                    break;
                default:
                    break;
            }

            switch (gravity)
            {
                case GravityDirection.Haut:
                    Body.Node.Rotation2D = 0;
                    Sprite.FlipY = true;
                    break;
                case GravityDirection.Bas:
                    Body.Node.Rotation2D = 0;
                    Sprite.FlipY = false;
                    break;
                case GravityDirection.Gauche:
                    Body.Node.Rotation2D = 270;
                    Sprite.FlipY = false;
                    break;
                case GravityDirection.Droite:
                    Sprite.FlipY = true;
                    Body.Node.Rotation2D = 270;
                    break;
                default:
                    break;
            }


            if (gravity == GravityDirection.Droite || gravity == GravityDirection.Gauche)
            {
                (vect.X, vect.Y) = (vect.Y, -vect.X);
            }

            this.IsGroundedQ();
            Body.SetLinearVelocity((vect + GameManager.Current.Scene.PhysicsWorld2D.Gravity) * Speed);

        }



        public void Move(Vector2 position)
        {
            Node.SetPosition2D(position);
        }

        public override void Update(UpdateEventArgs obj)
        {
            internalTimer = (internalTimer + obj.TimeStep) % 1000;
            if (GameManager.Current.Scene.UpdateEnabled)
                Sprite.Sprite = Sprites[((int)(internalTimer * 2)) % Sprites.Count];
            if (Body.LinearVelocity.Length > 5f)
            {
                Body.SetLinearVelocity(Body.LinearVelocity * 0.90f);
            }
        }

        public bool IsGroundedQ()
        {
            var gravity = GameManager.Current.Scene.Gravity;

            IsGrounded = ((gravity == GravityDirection.Droite || gravity == GravityDirection.Gauche) && 0 == Body.LinearVelocity.X)
                || ((gravity == GravityDirection.Haut || gravity == GravityDirection.Bas) && 0 == Body.LinearVelocity.Y);
            return IsGrounded;
        }
        public void Respawn()
        {
            IsGrounded = false;
            if (LastCheckpoint != null)
            {
                GameManager.Current.Scene.Gravity = LastCheckpoint.SpawnGravity;
                Body.Node.Position = new Vector3(LastCheckpoint.Position.X, LastCheckpoint.Position.Y, Body.Node.Position.Z);
            }
        }

        public void setSpawn(Checkpoint cp)
        {
            LastCheckpoint = cp;
        }
        public void Dies()
        {
            Respawn();
        }
    }
}

