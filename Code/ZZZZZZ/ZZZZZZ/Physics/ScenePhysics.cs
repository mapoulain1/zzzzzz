﻿using Urho;
using Urho.Urho2D;

namespace ZZZZZZ.Physics
{
    public class ScenePhysics : Scene
    {
        public PhysicsWorld2D PhysicsWorld2D { get; private set; }
        public CollisionHandler CollisionHandler { get; private set; }

        private GravityDirection gravity;

        public GravityDirection Gravity
        {
            get { return gravity; }
            set
            {
                gravity = value;
                switch (gravity)
                {
                    case GravityDirection.Bas:
                        PhysicsWorld2D.Gravity = -Vector2.UnitY;
                        break;
                    case GravityDirection.Haut:
                        PhysicsWorld2D.Gravity = Vector2.UnitY;
                        break;
                    case GravityDirection.Gauche:
                        PhysicsWorld2D.Gravity = -Vector2.UnitX;
                        break;
                    case GravityDirection.Droite:
                        PhysicsWorld2D.Gravity = Vector2.UnitX;
                        break;
                }
            }
        }

        public ScenePhysics() : base()
        {
            CreateComponent<Octree>();
            PhysicsWorld2D = CreateComponent<PhysicsWorld2D>();
            Gravity = GravityDirection.Bas;
            CollisionHandler = CreateComponent<CollisionHandler>();
            CollisionHandler.PhysicsWorld2D = PhysicsWorld2D;
            CreateComponent<DebugRenderer>();
            UpdateEnabled = true;
            
            
        }


    }
}
