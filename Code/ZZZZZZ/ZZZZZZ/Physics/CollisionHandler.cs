﻿using System;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Physics.CollisionBoxs;
using ZZZZZZ.Worlds.Portals;

namespace ZZZZZZ.Physics
{
    public class CollisionHandler : Component
    {
        private PhysicsWorld2D _physicsWorld2D;
        public event Action<CollisionBoxTriggeredEventArgs> CollisionBoxTriggered;
        public event Action<AreaTriggeredEventArgs> AreaLevelTriggered;
        public event Action<PortalTriggeredEventArgs> PortalTriggered;


        public PhysicsWorld2D PhysicsWorld2D
        {
            get => _physicsWorld2D;
            set
            {
                if (_physicsWorld2D != value)
                {
                    _physicsWorld2D = value;
                    _physicsWorld2D.PhysicsBeginContact2D += PhysicsWorld2D_PhysicsBeginContact2D;
                    _physicsWorld2D.PhysicsEndContact2D += PhysicsWorld2D_PhysicsEndContact2D;
                }
            }
        }

        private void PhysicsWorld2D_PhysicsBeginContact2D(PhysicsBeginContact2DEventArgs obj)
        {
            LocalCharacter perso = null;

            var cb = GetSenderBegin<CollisionBox>(obj);
            var area = GetSenderBegin<Area>(obj);
            var portal = GetSenderBegin<Portal>(obj);
            var spike = GetSenderBegin<Spike>(obj);
            var cp = GetSenderBegin<Checkpoint>(obj);

            if (cb != null)
                CollisionBoxTriggered?.Invoke(new CollisionBoxTriggeredEventArgs(cb.Goto));
            if (area != null)
                AreaLevelTriggered?.Invoke(new AreaTriggeredEventArgs(area.WorldPosition));
            if (portal != null)
                PortalTriggered?.Invoke(new PortalTriggeredEventArgs(portal.PortalID, portal.In));


            if (obj.BodyA == (GameManager.Current.LocalPlayerManager.Character as LocalCharacter).Body)
                perso = GameManager.Current.LocalPlayerManager.Character as LocalCharacter;
            if (obj.BodyB == (GameManager.Current.LocalPlayerManager.Character as LocalCharacter).Body)
                perso = GameManager.Current.LocalPlayerManager.Character as LocalCharacter;


            if (perso != null)
            {
                if (spike !=null)
                    perso.Dies();
                if(cp != null && cp.Status ==false)
                {
                    //cp.On //animation quand on est dessus
                    cp.On();
                    perso.setSpawn(cp);

                }
            }


        }


        private T GetSenderBegin<T>(PhysicsBeginContact2DEventArgs obj) where T : class
        {
            T sender = null;
            if (obj.BodyA.GetType() == typeof(T)) sender = obj.BodyA as T;
            if (obj.BodyB?.GetType() == typeof(T)) sender = obj.BodyB as T;
            return sender;
        }
        private T GetSenderEnd<T>(PhysicsEndContact2DEventArgs obj) where T : class
        {
            T sender = null;
            if (obj.BodyA.GetType() == typeof(T)) sender = obj.BodyA as T;
            if (obj.BodyB?.GetType() == typeof(T)) sender = obj.BodyB as T;
            return sender;
        }

        private void PhysicsWorld2D_PhysicsEndContact2D(PhysicsEndContact2DEventArgs obj)
        {
            var cp = GetSenderEnd<Checkpoint>(obj);

            if(cp != null)
            {
                cp.Off();
            }
            /*
            LocalCharacter perso = null;

            if (obj.BodyA == (GameManager.Current.LocalPlayerManager.Character as LocalCharacter).Body)
                perso = GameManager.Current.LocalPlayerManager.Character as LocalCharacter;
            if (obj.BodyB == (GameManager.Current.LocalPlayerManager.Character as LocalCharacter).Body)
                perso = GameManager.Current.LocalPlayerManager.Character as LocalCharacter;
            if (perso != null)
            {
            }
            */
        }







    }
}
