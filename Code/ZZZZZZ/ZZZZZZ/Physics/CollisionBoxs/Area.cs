﻿using Urho;
using Urho.Physics;
using Urho.Urho2D;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class Area : RigidBody2D
    {
        public Vector2 WorldPosition { get; private set; }
        public Area Init(Vector2 worldPosition)
        {
            WorldPosition = worldPosition;
            var box = Node.CreateComponent<CollisionBox2D>();
            box.Size = new Vector2(736, 448) / 100;
            box.Trigger = true;
            box.Node.SetPosition2D(new Vector2(16, 16) / 100 + box.Size / 2);
            return this;
        }


    }
}
