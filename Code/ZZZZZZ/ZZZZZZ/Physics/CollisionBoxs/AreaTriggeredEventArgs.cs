﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class AreaTriggeredEventArgs : EventArgs
    {
        public Vector2 CameraPosition { get;  }
        public AreaTriggeredEventArgs(Vector2 cameraPosition)
        {
            CameraPosition = cameraPosition;
        }
    }
}
