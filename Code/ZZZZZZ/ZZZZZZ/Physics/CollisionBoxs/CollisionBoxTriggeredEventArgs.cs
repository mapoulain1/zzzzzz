﻿using System;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class CollisionBoxTriggeredEventArgs : EventArgs
    {
        public string Goto { get; }

        public CollisionBoxTriggeredEventArgs(string @goto)
        {
            this.Goto = @goto;
        }
    }
}
