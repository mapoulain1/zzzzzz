﻿using Urho;
using Urho.Urho2D;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class CollisionBox : RigidBody2D
    {
        public string Goto { get => Tile.GetProperty("goto"); }
        
        private TileMapObject2D Tile { get; set; }

        public CollisionBox Init(TileMapObject2D tile)
        {
            Tile = tile;
            var box = Node.CreateComponent<CollisionBox2D>();
            box.Size = tile.Size;
            box.Trigger = true;
            box.Node.SetPosition2D(tile.Position + box.Size / 2);
            return this;
        }


    }
}
