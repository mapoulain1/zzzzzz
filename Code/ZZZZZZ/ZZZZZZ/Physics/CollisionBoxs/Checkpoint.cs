﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class Checkpoint : RigidBody2D
    {
        public Vector2 Position { get; private set; }
        public bool Status { get; private set; }
        public GravityDirection SpawnGravity { get; private set; }
        public Sprite2D OnSprite { get; private set; }
        public Checkpoint Init(Vector2 position)
        {
            SpawnGravity = GravityDirection.Bas;
            var box = Node.CreateComponent<CollisionCircle2D>();
            box.Radius = 8f/100;
            box.Trigger = true;
            box.Node.SetPosition2D(position + new Vector2(8, 8) / 100);
            Position = box.Node.Position2D;
            Status = false;
            //OnSprite = Application.ResourceCache.GetSprite2D(Assets.Sprites.Checkpoint.CheckpointOn);
            
            return this;
        }
        public void On()
        {
            Status = true;
        }
        public void Off()
        {
            Status = false;
        }
    }
}

