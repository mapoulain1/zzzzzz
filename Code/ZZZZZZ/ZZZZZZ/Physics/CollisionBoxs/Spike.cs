﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Urho2D;

namespace ZZZZZZ.Physics.CollisionBoxs
{
    public class Spike : RigidBody2D
    {
        public Vector2 Position { get; private set; }
        public Spike Init(Vector2 position)
        {
            var box = Node.CreateComponent<CollisionBox2D> ();
            box.Size = new Vector2(16f, 16f) / 100;
            box.Trigger = true;
            box.Node.SetPosition2D(position + new Vector2(8, 8) / 100);
            Position = box.Center;
            return this;
        }
    }
}
