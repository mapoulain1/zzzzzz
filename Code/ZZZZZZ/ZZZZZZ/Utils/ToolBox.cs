﻿using System;
using System.Net;
using System.Net.Sockets;
using Urho;
using ZZZZZZ.Inputs;
using ZZZZZZ.Objects.Character;

namespace ZZZZZZ.Utils
{
    public static class ToolBox
    {

        public static Random Random { get; } = new Random();


        public static float RandomFloat(float min, float max)
        {
            return (float)(min + (max - min) * Random.NextDouble());
        }

        public static float RandomFloat(float max)
        {
            return RandomFloat(0, max);
        }

        public static float RandomFloat(double min, double max)
        {
            return RandomFloat((float)min, (float)max);
        }

        public static float RandomFloat(double max)
        {
            return RandomFloat(0f, (float)max);
        }

        public static bool IsKeyDown(Key key, bool continuous)
        {
            return continuous ? Application.Current.Input.GetKeyDown(key) : Application.Current.Input.GetKeyPress(key);

        }

        public static Vector2 DegreeToVectorNomalized(float degree)
        {
            return new Vector2((float)DCos(degree), (float)DSin(degree));
        }


        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        public static double RadianToDegree(double angle)
        {
            return angle * 180.0 / Math.PI;
        }

        public static int FontSize(string text, int width, int em = 30)
        {
            return Math.Min(FontSizeRelative(em), width / Math.Max(1, text.Length));
        }

        public static int FontSizeRelative(int size)
        {
            return Application.Current.Graphics.Width / size;
        }

        public static IntVector2 ScreenPosition(float percentX, float percentY)
        {
            return new IntVector2((int)(Application.Current.Graphics.Width * percentX), (int)(Application.Current.Graphics.Height * percentY));
        }




        public static double DCos(double angle)
        {
            return Math.Cos(DegreeToRadian(angle));
        }
        public static double DSin(double angle)
        {
            return Math.Sin(DegreeToRadian(angle));
        }

        public static double BounceAnimation(int step)
        {
            return (1 / 1350.0) * step * step - (1 / 22.5) * step + 1;
        }

        public static IntVector2 WindowSize()
        {
            return new IntVector2(Application.Current.Graphics.Width, Application.Current.Graphics.Height);
        }

        public static IntVector2 PercentToPixel(float percentW, float percentH)
        {
            return new IntVector2((int)(Application.Current.Graphics.Width * percentW), (int)(Application.Current.Graphics.Height * percentH));
        }

        public static IntVector2 PercentToPixelButton(float scale)
        {
            var width = scale * Application.Current.Graphics.Width;
            var height = width / 2;
            return new IntVector2((int)width, (int)height);
        }



        public static bool IsPosInRect(int x, int y, IntRect rect)
        {
            return x >= rect.Left && x <= rect.Right && y >= rect.Top && y <= rect.Bottom;
        }

        public static decimal Map(decimal value, decimal fromSource, decimal toSource, decimal fromTarget, decimal toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }

        public static int AvailableUDPPort()
        {
            var udpClient = new UdpClient(0);
            var availableUDPPort = ((IPEndPoint)udpClient.Client.LocalEndPoint).Port;
            udpClient.Close();
            return availableUDPPort;
        }

        public static (Vector2 position, (bool x, bool y))? TryParseQuerry(string querry)
        {
            bool flipX;
            bool flipY;
            var splitted = querry.Split(' ');
            if (splitted.Length < 4)
            {
                Console.WriteLine($"Error {splitted.Length} : {querry}");
                return null;
            }
            if (!float.TryParse(splitted[0], out float x)) x = 0;
            if (!float.TryParse(splitted[1], out float y)) y = 0;
            flipX = splitted[2] != "0";
            flipY = splitted[3] == "0";
            return (new Vector2(x, y), (flipX, flipY));
        }

        public static string GenerateQuerry(LocalCharacter character)
        {
            return $"{character.Node.Position2D.X} {character.Node.Position2D.Y} {(character.Flip.X ? '1' : '0')} {(character.Flip.Y ? '1' : '0')} ";
        }

        public static (string ip, int port) ExtractConnectionString(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Connection string can't be empty");
            var splitted = connectionString.Split(':');
            if (splitted.Length < 2)
                throw new ArgumentException("Connection string can't be parsed");
            var ip = splitted[0];
            var port = int.Parse(splitted[1]);
            return (ip, port);
        }
        public static AbstractInput AdaptedInput()
        {
            var rt = System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription;
            if (rt.Contains("Framework"))
                return new KeyboardInput();
            if (rt.Contains("Mono"))
                return new JoystickInput();
            return new JoystickInput();
        }
    }
}
