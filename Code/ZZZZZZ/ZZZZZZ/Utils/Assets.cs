﻿namespace ZZZZZZ.Utils
{
    public class Assets
    {
        public static class PostProcess
        {
            public const string AutoExposure = "PostProcess/auto_exposure.xml";
            public const string Bloom = "PostProcess/bloom.xml";
            public const string BloomHDR = "PostProcess/bloom_hdr.xml";
            public const string Blur = "PostProcess/blur.xml";
            public const string ColorCorrection = "PostProcess/color_correction.xml";
            public const string FXAA2 = "PostProcess/fxaa2.xml";
            public const string FXAA3 = "PostProcess/fxaa3.xml";
            public const string GammaCorrection = "PostProcess/gamma_correction.xml";
            public const string GreyScale = "PostProcess/grey_scale.xml";
            public const string Tonemap = "PostProcess/tonemap.xml";
        }

        public static class Fonts
        {
            public const string AnonymousPro = "Fonts/anonymous_pro.ttf";
        }

        public static class Levels
        {
            public const string BasePath = "Levels/";
            public const string World0Path = "Levels/World0/";
            public const string World0Start = "Levels/World0/world0_0!0.tmx";
            public const string World1Path = "Levels/World1/";
            public const string World1Start = "Levels/World1/world1_0!0.tmx";

        }

        public static class UI
        {
            public const string JoystickInner = "UI/inner_joystick.png";
            public const string JoystickOuter = "UI/outer_joystick.png";
            public static class Menus
            {
                public const string Intro = "UI/Menus/intro.xml";
                public const string Main = "UI/Menus/main_menu.xml";
                public const string Multiplayer = "UI/Menus/multiplayer_menu.xml";
                public const string Singleplayer = "UI/Menus/singleplayer_menu.xml";
                public const string Option = "UI/Menus/option_menu.xml";
                public const string Ingame = "UI/Menus/ingame_hud.xml";

                public static class Images
                {

                    public const string ButtonBackground = "UI/Menus/Buttons/button.png";
                    public const string OpacityBackground = "UI/Menus/Images/opacity_background.png";
                    public const string IconPath = "UI/Menus/Images/";
                }
            }
        }


        public static class Sprites
        {
            public static class Characters
            {
                public const string CharacterLocal_1 = "Sprites/Characters/character_1.png";
                public const string CharacterLocal_2 = "Sprites/Characters/character_2.png";

                public const string CharacterRemote_1 = "Sprites/Characters/character_remote_1.png";
                public const string CharacterRemote_2 = "Sprites/Characters/character_remote_2.png";
            }

            public static class Checkpoint
            {
                public const string CheckpointOn = "Sprites/Checkpoint/checkpointOn.png";
            }

            public static class Worlds
            {
                public static class Teleporter
                {
                    public const string TeleporterIn1 = "Sprites/Worlds/Teleporters/portal_in_1.png";
                    public const string TeleporterIn2 = "Sprites/Worlds/Teleporters/portal_in_2.png";
                    public const string TeleporterIn3 = "Sprites/Worlds/Teleporters/portal_in_3.png";
                    public const string TeleporterOut1 = "Sprites/Worlds/Teleporters/portal_out_1.png";
                    public const string TeleporterOut2 = "Sprites/Worlds/Teleporters/portal_out_2.png";
                    public const string TeleporterOut3 = "Sprites/Worlds/Teleporters/portal_out_3.png";
                }
            }

        }

        public static class Particles
        {
            public const string Background = "Particles/background.pex";
        }


        public static class Sounds
        {
            public const string Alarm = "Sounds/alarm.wav";
            public const string Bioup = "Sounds/bioup.wav";
            public const string Bup = "Sounds/bup.wav";
            public const string Didadidadou = "Sounds/didadidadou.wav";
            public const string Jump = "Sounds/jump.wav";
            public const string Notification = "Sounds/notification.wav";
            public const string Sad = "Sounds/sad.wav";
        }
        public static class Musics
        {
            public const string Menu = "Musics/menu.ogg";
            public const string Game1 = "Musics/game_1.ogg";
            public const string Game2 = "Musics/game_2.ogg";
            public const string Game3 = "Musics/game_3.ogg";
        }

    }
}
