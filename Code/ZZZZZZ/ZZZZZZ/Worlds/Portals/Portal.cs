﻿using System;
using Urho;
using Urho.Actions;
using Urho.Urho2D;
using ZZZZZZ.Utils;
using static ZZZZZZ.Utils.Assets.Sprites.Worlds.Teleporter;

namespace ZZZZZZ.Worlds.Portals
{
    public class Portal : RigidBody2D
    {
        public int PortalID { get; private set; }
        public bool In { get; private set; }
        public Vector2 Position { get; private set; }
        
        public event Action<PortalTriggeredEventArgs> PortalTriggered;


        public Portal Init(Vector2 position, int id, bool _in)
        {
            PortalID = id;
            In = _in;
            var box = Node.CreateComponent<CollisionCircle2D>();
            box.Radius = 32f / 100;
            box.Trigger = true;
            box.Node.SetPosition2D(position + new Vector2(32, 32) / 100);
            Position = new Vector2(box.Node.WorldPosition.X, box.Node.WorldPosition.Y);
            GameManager.Current.Scene.CollisionHandler.PortalTriggered += CollisionHandler_PortalTriggered;



            var sprite1 = Node.CreateChild().CreateComponent<StaticSprite2D>();
            sprite1.Sprite = Application.Current.ResourceCache.GetSprite2D(In ? TeleporterIn1 : TeleporterOut1);
            sprite1.Alpha = 0.8f;
            sprite1.Node.Scale = 0.4f * Vector3.One;

            var sprite2 = Node.CreateChild().CreateComponent<StaticSprite2D>();
            sprite2.Sprite = Application.Current.ResourceCache.GetSprite2D(In ? TeleporterIn2 : TeleporterOut2);
            sprite2.Alpha = 0.25f;
            sprite2.Node.Scale = 0.7f * Vector3.One;

            var sprite3 = Node.CreateChild().CreateComponent<StaticSprite2D>();
            sprite3.Sprite = Application.Current.ResourceCache.GetSprite2D(In ? TeleporterIn3 : TeleporterOut3);
            sprite3.Alpha = 0.1f;
            sprite3.Node.Scale = 1.0f * Vector3.One;



            sprite1.Node.RunActionsAsync(new Repeat(new RotateBy(2, 0, 0, In ? 360 : -360), 10000));
            sprite2.Node.RunActionsAsync(new Repeat(new RotateBy(2, 0, 0, In ? 90 : -90), 10000));
            sprite3.Node.RunActionsAsync(new Repeat(new RotateBy(2, 0, 0, In ? 45 : -45), 10000));

            return this;
        }

        private void CollisionHandler_PortalTriggered(PortalTriggeredEventArgs obj)
        {
            if (PortalID == obj.Id && In == obj.In && In)
                PortalTriggered?.Invoke(obj);
        }
    }
}
