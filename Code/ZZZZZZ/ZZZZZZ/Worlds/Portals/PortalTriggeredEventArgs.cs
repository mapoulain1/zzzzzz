﻿using System;

namespace ZZZZZZ.Worlds.Portals
{
    public class PortalTriggeredEventArgs : EventArgs
    {
        public int Id { get; }
        public bool In { get; }

        public PortalTriggeredEventArgs(int id, bool _in)
        {
            Id = id;
            In = _in;
        }
    }
}
