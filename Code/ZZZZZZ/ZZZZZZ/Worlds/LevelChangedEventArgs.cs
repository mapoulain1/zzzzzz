﻿using System;
using Urho;

namespace ZZZZZZ.Worlds
{
    public class LevelChangedEventArgs : EventArgs
    {
        public Vector2 SpawnLocation { get; }
        public string Name { get; }
        public LevelChangedEventArgs(string name, Vector2 spawnLocation)
        {
            SpawnLocation = spawnLocation;
            Name = name;
        }
    }
}
