﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Urho;
using Urho.Actions;
using ZZZZZZ.Physics;
using ZZZZZZ.Physics.CollisionBoxs;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Worlds
{
    public class LevelManager
    {
        public Level Current { get; private set; }
        public List<Level> Loaded { get; }

        public event Action<LevelChangedEventArgs> LevelChanged;


        public LevelManager(CollisionHandler collisionHandler)
        {
            collisionHandler.CollisionBoxTriggered += CollisionHandler_CollisionBoxTriggered;
            collisionHandler.AreaLevelTriggered += CollisionHandler_AreaLevelTriggered;
            Loaded = new List<Level>();
        }

        private void CollisionHandler_AreaLevelTriggered(AreaTriggeredEventArgs obj)
        {
            Console.WriteLine($"entering area {obj.CameraPosition}");
            GameManager.Current.Camera.Node.Position = new Vector3(obj.CameraPosition.Y * 7.68f + 3.84f, obj.CameraPosition.X * 4.8f + 2.4f, 0);
            Current = Loaded.Find(x => x.WorldX == obj.CameraPosition.X && x.WorldY == obj.CameraPosition.Y);

            var regex = new Regex(@"world(-?\d+)_(-?\d+)!(-?\d+).tmx");

            var match = regex.Match(Current.Name).Groups;
            if (match.Count >= 4)
            {
                var toLoad = Path.Combine(Path.GetDirectoryName(Current.Name), $"world{match[1]}_{match[2]}!{match[3]}.tmx");
                Unload();
                Load(toLoad);
            }
            else
            {
                Console.WriteLine($"problem with on fly loading of world {Current.Name}");
            }

        }

        public void Unload()
        {
            var toDelete = Loaded.Select(x => (x.WorldX, x.WorldY)).ToList();
            for (int i = Current.WorldX - 1; i <= Current.WorldX + 1; i++)
                for (int j = Current.WorldY - 1; j <= Current.WorldY + 1; j++)
                    toDelete.Remove((i, j));
            foreach (var (WorldX, WorldY) in toDelete)
            {
                Console.WriteLine($"unloading {WorldX} {WorldY}");
                var toDispose = Loaded.Find(x => x.WorldX == WorldX && x.WorldY == WorldY);
                toDispose.Dispose();
                Loaded.Remove(toDispose);
            }

        }

        public void Dispose()
        {
            foreach (var level in Loaded)
                level.Dispose();
            Loaded.Clear();
            Current = null;
        }


        public void Load(string level)
        {
            var path = Path.GetDirectoryName(level);

            var regex = new Regex(@"world(-?\d+)_(-?\d+)!(-?\d+).tmx");
            var match = regex.Match(level);
            var groups = match.Groups;

            var levelX = int.Parse($"{groups[2]}");
            var levelY = int.Parse($"{groups[3]}");

            for (int i = levelX - 1; i <= levelX + 1; i++)
            {
                for (int j = levelY - 1; j <= levelY + 1; j++)
                {
                    if (!Loaded.Exists(x => x.WorldX == i && x.WorldY == j))
                    {
                        var filePathToLoad = Path.Combine(path, $"world{groups[1]}_{i}!{j}.tmx");
                        var tmxLevel = Application.Current.ResourceCache.GetTmxFile2D(filePathToLoad, false);
                        if (tmxLevel != null)
                        {
                            var _level = new Level(filePathToLoad, i, j);
                            Loaded.Add(_level);
                            if (i == levelX && j == levelY)
                                Current = _level;
                        }
                    }
                }
            }
        }

        private void CollisionHandler_CollisionBoxTriggered(CollisionBoxTriggeredEventArgs obj)
        {
        }
    }
}
