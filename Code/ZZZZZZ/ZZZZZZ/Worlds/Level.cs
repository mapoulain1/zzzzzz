﻿using System;
using System.Collections.Generic;
using System.Linq;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Physics.CollisionBoxs;
using ZZZZZZ.Worlds.Portals;

namespace ZZZZZZ.Worlds
{
    public class Level : Component
    {

        public TileMap2D TileMap { get; private set; }
        public string Name { get; private set; }
        public Vector2? Spawn { get; private set; }
        public List<Portal> Portals { get; private set; }
        public List<Spike> Spikes { get; private set; }
        public List<Checkpoint> Checkpoints { get; private set; }
        public Area Area { get; private set; }
        public int WorldX { get; private set; }
        public int WorldY { get; private set; }
        private Node ChildFromScene { get; }



        public Level(string level, int x, int y)
        {
            Portals = new List<Portal>();
            Spikes = new List<Spike>();
            Checkpoints = new List<Checkpoint>();
            WorldX = x;
            WorldY = y;

            Name = level;
            ChildFromScene = GameManager.Current.Scene.CreateChild("Level");
            ChildFromScene.AddComponent(this);

            TileMap = Node.CreateComponent<TileMap2D>();
            TileMap.TmxFile = Application.ResourceCache.GetTmxFile2D(level);
            TileMap.Node.SetPosition2D(new Vector2(y * 7.68f, x * 4.8f));

            Area = Node.CreateChild().CreateComponent<Area>().Init(new Vector2(WorldX, WorldY));

            GenerateTriggerBox();
            GenerateCollision();
            GenerateObjects();

        }

        private void GenerateObjects()
        {
            var layer = TileMap.GetLayer(1);
            if (layer == null) return;
            for (uint i = 0; i < layer.NumObjects; i++)
            {
                switch (layer.GetObject(i).Name)
                {
                    case "Checkpoint":
                        Node nodeCp = Node.CreateChild("Checkpoint");
                        var cp = nodeCp.CreateComponent<Checkpoint>().Init(layer.GetObject(i).Position);
                        Checkpoints.Add(cp);
                        break;

                    case "Spike":
                        Node nodeSpike = Node.CreateChild("Spike");
                        var spike = nodeSpike.CreateComponent<Spike>().Init(layer.GetObject(i).Position);
                        Spikes.Add(spike);
                        break;

                    case "Portal":
                        var objPortal = layer.GetObject(i);
                        var id = int.Parse(objPortal.GetProperty("id"));
                        var _in = bool.Parse(objPortal.GetProperty("in"));
                        Node nodePortal = Node.CreateChild("Portal");
                        var portal = nodePortal.CreateComponent<Portal>().Init(objPortal.Position, id, _in);
                        Portals.Add(portal);
                        portal.PortalTriggered += Portal_PortalTriggered;
                        break;

                    case "Spawn":
                        var obj = layer.GetObject(i);
                        Node nodeSpawn = Node.CreateChild("tmp");
                        nodeSpawn.SetPosition2D(obj.Position);
                        Spawn = nodeSpawn.WorldPosition2D;
                        nodeSpawn.Dispose();
                        break;
                    default:
                        break;
                }
            }
        }


        private void Portal_PortalTriggered(PortalTriggeredEventArgs obj)
        {
            var portalIn = Portals.Find(x => x.PortalID == obj.Id && x.In == obj.In);
            if (portalIn != null && portalIn.In)
            {
                var portalOut = Portals.Find(x => x.PortalID == obj.Id && x.In != obj.In);
                if (portalOut != null)
                {
                    Console.WriteLine($"teleport player to {portalOut.Position}");
                    GameManager.Current.LocalPlayerManager.Character.Move(portalOut.Position);
                }
            }
        }


        private void GenerateTriggerBox()
        {
            var layer = TileMap.GetLayer(1);
            if (layer == null) return;
            for (uint i = 0; i < layer.NumObjects; i++)
            {
                if (layer.GetObject(i).Name == "Goto")
                {
                    Node node = Node.CreateChild("TriggerBox");
                    node.CreateComponent<CollisionBox>().Init(layer.GetObject(i));
                }
            }
        }

        private void GenerateCollision()
        {
            var layer = TileMap.GetLayer(0);
            var size = CalculateBoxSize(layer.Width);
            for (int i = 0; i < layer.Width; i++)
            {
                for (int j = 0; j < layer.Width; j++)
                {
                    var tile = layer.GetTile(i, j);
                    if (tile != null)
                    {
                        Node node = Node.CreateChild("RigidBody");
                        node.CreateComponent<RigidBody2D>();
                        CollisionBox2D box = node.CreateComponent<CollisionBox2D>();
                        box.Size = size;
                        box.Node.SetPosition2D(TileMap.TileIndexToPosition(i, j) + size / 2);
                    }
                }
            }
        }


        protected override void Dispose(bool disposing)
        {
            try
            {
                foreach (var portal in Portals)
                    portal.Dispose();

                GameManager.Current.Scene.RemoveChild(ChildFromScene);
                base.Dispose(disposing);
            }
            catch (Exception) { }
        }

        private Vector2 CalculateBoxSize(int width)
        {
            if (width == 24)
                return Vector2.One * 32 / 100;
            return Vector2.One * 16 / 100;
        }
    }
}
