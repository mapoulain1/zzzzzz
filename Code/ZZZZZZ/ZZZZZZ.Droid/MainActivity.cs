﻿using Android.App;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using AndroidX.AppCompat.App;
using Urho.Droid;

namespace ZZZZZZ.Droid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true, ScreenOrientation = ScreenOrientation.SensorLandscape)]
    public class MainActivity : AppCompatActivity
    {
        [System.Obsolete]
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            var height = Xamarin.Essentials.DeviceDisplay.MainDisplayInfo.Height;
            var width = Xamarin.Essentials.DeviceDisplay.MainDisplayInfo.Width;
            var gameWidth = height * 16 / 10;


            var mLayout = new AbsoluteLayout(this);
            var urhoSurface = UrhoSurface.CreateSurface(this);
            mLayout.AddView(urhoSurface);
            mLayout.SetBackgroundColor(Color.Black);
            SetContentView(mLayout);

            
            urhoSurface.Left = (int)((width - gameWidth) / 2);
            urhoSurface.LayoutParameters = new AbsoluteLayout.LayoutParams((int)gameWidth, (int)height, (int)((width - gameWidth) / 2), 0);


            var _appOptions = new Urho.ApplicationOptions("Data")
            {
                LimitFps = true,
                NoSound = false
            };

            urhoSurface.Show(typeof(GameManager), _appOptions);


        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}