require "socket"

$global_main_port_listening = 0
$global_number_clients = 0
$global_clients = Array.new(2)

$global_clients[0] = "0 0"
$global_clients[1] = "0 0"

def usage
  puts "usage :"
  puts "ruby server.rb [port]"
end

def startListening
  puts "listening on #{Socket.ip_address_list.detect { |intf| intf.ipv4_private? }.ip_address}:#{$global_main_port_listening}"

  listener = TCPServer.open($global_main_port_listening)
  while true
    puts "waiting for client"
    loop {
      Thread.start(listener.accept) do |client|
        $global_number_clients += 1
        serverUDPPort = $global_main_port_listening + $global_number_clients

        portC, ipC = Socket.unpack_sockaddr_in(client.getpeername)
        puts "client arrived from #{ipC}"
        clientUDPPort = Integer(client.gets)
        puts "client listening on UDP port #{clientUDPPort}"
        client.puts "#{serverUDPPort}"
        puts "server listening on UDP port #{serverUDPPort}"
        puts $global_number_clients
        #while $global_number_clients != 2
        #  if client.closed?
        #    $global_number_clients -= 1
        #    return
        #  end
        #  sleep(0.1)
        #end
        start(serverUDPPort, clientUDPPort, ipC, $global_number_clients - 1, client)
        $global_number_clients -= 1
      end
    }
  end
end

def start(portServer, portClient, ipClient, id, tcpConnection)
  receiveFrom = UDPSocket.new
  receiveFrom.bind("0.0.0.0", portServer)
  sendTo = UDPSocket.new
  sendTo.connect(ipClient, portClient)
  while msg = receiveFrom.gets #&& !tcpConnection.eof?
    $global_clients[id] = msg.strip
    sendTo.send $global_clients[(id + 1) % 2], 0
  end
  receiveFrom.close
  sendTo.close
end

def handleCommandLine
  if ARGV.length != 1
    usage
  else
    $global_main_port_listening = Integer(ARGV[0])
    startListening
  end
end

def showClients
  while true
    print "#{$global_number_clients} : (#{$global_clients[0]}) (#{$global_clients[1]})\n"
    sleep(0.1)
  end
end

Thread.new { showClients }
handleCommandLine
