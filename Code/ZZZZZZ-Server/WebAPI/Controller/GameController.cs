﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebAPI.Model;

namespace WebAPI.Controller
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {

        [HttpPost("CreateGame")]
        public string CreateGame()
        {
            var game = new Game(Utils.RandomString(GameManager.Instance.Games.Select(x => x.Code).ToList(), 3));
            GameManager.Instance.Games.Add(game);
            Trace.WriteLine($"New Game <{game.Code}>");
            return game.Code;
        }

        [HttpPost("JoinGame")]
        public int JoinGame(string code)
        {
            var game = Find(code);
            if (game == null)
                return -1;
            var id = game.AddPlayer();
            Trace.WriteLine($"Player <{id}> joined Game <{game.Code}>");
            return id;
        }

        [HttpPut("SendPlayer")]
        public IActionResult SendPlayer(string code, int id, float x, float y, bool flipX, bool flipY, int rotation)
        {
            var game = Find(code);
            if (game == null)
                return BadRequest("Game not found");
            if (!game.Ready)
                return BadRequest("Game not started"); ;
            var player = game.Players.Find(x => x.Id == id);
            if (player != null)
            {
                player.X = x;
                player.Y = y;
                player.FlipX = flipX;
                player.FlipY = flipY;
                player.Rotation = rotation;
            }

            return Ok();
        }


        [HttpGet("GetPlayer")]
        public Player GetPlayer(string code, int ownId)
        {
            var game = Find(code);
            if (game == null)
                return null;
            if (!game.Ready)
                return null;
            var otherPlayer = game.Players.Find(x => x.Id != ownId);
            return otherPlayer;
        }


        [HttpGet("IsGameReady")]
        public bool IsGameReady(string code)
        {
            var game = Find(code);
            if (game == null)
                return false;
            return game.Ready;
        }

        [HttpDelete("RemovePlayer")]
        public IActionResult RemovePlayer(string code, int id)
        {
            var game = Find(code);
            if (game == null)
                return BadRequest("Game not found");
            var player = game.Players.Find(x => x.Id == id);
            if (player != null)
                game.Players.Remove(player);
            return Ok();
        }

        [HttpGet("GetPlayers")]
        public IEnumerable<Player> GetPlayer(string code)
        {
            var game = Find(code);
            if (game == null)
                return Enumerable.Empty<Player>();
            return game.Players;
        }


        [HttpGet("GetGameCodes")]
        public IEnumerable<string> GetGameCodes()
        {
            return GameManager.Instance.Games.Select(x => x.Code).ToList();
        }


        [HttpGet("GetGameAdmin")]
        public IEnumerable<GameAdmin> GetGameAdmin()
        {
            return GameManager.Instance.GamesAdmins;
        }

        [HttpDelete("StopGame")]
        public IActionResult StopGame(string code)
        {
            try
            {
                var game = Find(code);
                if (game == null)
                    return BadRequest("Game not found");
                GameManager.Instance.Games.Remove(game);
                Trace.WriteLine($"Game <{code}> removed");
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Game not found");
            }
        }


        private static Game Find(string code)
        {
            return GameManager.Instance.Games.FirstOrDefault(x => x.Code == code);
        }

    }
}
