﻿using System.Diagnostics;

namespace WebAPI.Model
{
    public class Game
    {
        public string Code { get; }
        public List<Player> Players { get; }

        public bool Ready { get => Players.Count == 2; }

        public event Action<Game> NeedToBeCleaned;

        public Game(string code)
        {
            Code = code;
            Players = new List<Player>();
            CheckAFKPlayers();
            CheckEmptyGame();
        }

        

        public int AddPlayer()
        {
            if(Players.Count >= 2)
                return -1;
            var player = new Player(Players.Count);
            Players.Add(player);
            return player.Id;
        }

        private void CheckAFKPlayers()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    Task.Delay(3600000).Wait();
                    var afkPlayers = Players.Count(x => !x.Online);
                    Trace.WriteLine($"Cleaning {afkPlayers} afk players(s)");
                    Players.RemoveAll(x => x.Online == false);
                }
            });
        }


        private void CheckEmptyGame()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    Task.Delay(10000).Wait();
                    if (Players.Count == 0)
                        NeedToBeCleaned?.Invoke(this);
                }
            });
        }
    }
}
