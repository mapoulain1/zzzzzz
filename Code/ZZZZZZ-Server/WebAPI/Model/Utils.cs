﻿using System.Collections.ObjectModel;

namespace WebAPI.Model
{
    public static class Utils
    {
        public static Random Random { get; }
        private static readonly string chars = "ABCDEFGHJKLMPRSTUWXYZ23456789";


        static Utils()
        {
            Random = new Random();
        }

        public static string RandomString(List<string> codes, int length)
        {
            string code = string.Empty;
            int i = 0;
            do
            {
                code = new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Next(s.Length)]).ToArray());
                i++;
            } while (codes.Contains(code) || i < 50);
            return code;
        }

        public static int Remove<T>(this ObservableCollection<T> coll, Func<T, bool> condition)
        {
            var itemsToRemove = coll.Where(condition).ToList();

            foreach (var itemToRemove in itemsToRemove)
            {
                coll.Remove(itemToRemove);
            }

            return itemsToRemove.Count;
        }

    }
}
