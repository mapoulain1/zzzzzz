﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;

namespace WebAPI.Model
{
    public class GameManager
    {

        public ObservableCollection<Game> Games { get; }
        public List<GameAdmin> GamesAdmins { get; }

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameManager();
                return instance;
            }
        }

        private static GameManager instance = null;


        private GameManager()
        {
            Games = new ObservableCollection<Game>();
            GamesAdmins = new List<GameAdmin>();
            Games.CollectionChanged += Games_CollectionChanged;
        }

        private void Games_CollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems[0] is Game game)
                {
                    GamesAdmins.Add(new GameAdmin(game));
                    game.NeedToBeCleaned += Game_NeedToBeCleaned;
                }
            }
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Game game in e.OldItems)
                {
                    game.NeedToBeCleaned -= Game_NeedToBeCleaned;
                    GamesAdmins.RemoveAll(x => x.Code == game.Code);
                }
            }
        }

        private void Game_NeedToBeCleaned(Game obj)
        {
            Trace.WriteLine($"Cleaning 1 unused game out of {Games.Count}");
            Games.Remove(obj);
        }

    }
}
