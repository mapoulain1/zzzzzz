﻿namespace WebAPI.Model
{
    public class Player
    {
        public int Id { get; }
        public float X { get; set; }
        public float Y { get; set; }
        public bool FlipX { get; set; }
        public bool FlipY { get; set; }
        public bool Online { get; set; }
        public int Rotation { get; set; }


        public Player(int id)
        {
            Id = id;
            Online = true;
            CheckAFK();
        }

        private void CheckAFK()
        {
            Task.Run(() =>
            {
                while (true)
                {
                    var x = X;
                    var y = Y;
                    Task.Delay(30000).Wait();
                    Online = x != X || y != Y;
                }
            });
        }
    }
}
