﻿namespace WebAPI.Model
{
    public class GameAdmin
    {
        public string Code { get => basedOn.Code; }
        public int NumberOfPlayers { get => basedOn.Players.Count; }
        public bool Ready { get => basedOn.Ready; }


        private readonly Game basedOn;

        public GameAdmin(Game basedOn)
        {
            this.basedOn = basedOn;
        }
    }
}
