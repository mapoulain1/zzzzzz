import { API_PATH } from "../App";
import "./game-component.css";
export const Game = ({ game, callAPI }) => {
  return (
    <li key={game.code} className="list-group-item bg-dark rounded-0">
      <div className="d-flex flex-row justify-content-between">
        <div className="d-flex flex-row gameCode">
          <h2 className="text-white">{game.code}</h2>
          <span className={`badge bg-${game.ready ? "success" : "danger"} badge-pill m-3`}>{game.ready ? "Ready" : "Not ready"}</span>
        </div>
        <h3 className="text-white align-self-center">{game.numberOfPlayers}/2</h3>
        <button type="button" className="btn btn-danger gameStop" onClick={() => stopGameClicked(game.code, callAPI)}>
          Stop
        </button>
      </div>
    </li>
  );
};

const stopGameClicked = (code, callAPI) => {
  let request = new Request(`http://${API_PATH}/Game/StopGame?code=${code}`, {
    method: "DELETE",
  });
  fetch(request).then(callAPI());
};
