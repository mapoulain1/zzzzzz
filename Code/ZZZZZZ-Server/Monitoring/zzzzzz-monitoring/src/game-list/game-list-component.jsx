import { Game } from "../game/game-component";
import "./game-list-component.css";

export const GameList = ({ gameList, callAPI }) => {
  const games = gameList.map((x) => <Game game={x} callAPI={callAPI}></Game>);
  if (gameList.length === 0)
    return (
      <div className="alert alert-primary m-2" role="alert">
        No game is running now !
      </div>
    );
  return <ul className="list-group">{games}</ul>;
};
