import { useEffect, useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { Header } from "./header/header-component";
import { GameList } from "./game-list/game-list-component";

function App() {
  const [gameList, setGameList] = useState([]);

  const callAPI = () => {
    console.log("calling", Date.now());
    fetch(`http://${API_PATH}/Game/GetGameAdmin`)
      .then((response) => response.json())
      .then((json) => {
        setGameList(json);
      });
  };

  useEffect(() => {
    callAPI();
    setInterval(() => {
      callAPI();
    }, 1000);
  }, []);

  return (
    <div className="App">
      <Header></Header>
      <GameList gameList={gameList} callAPI={callAPI}></GameList>
    </div>
  );
}

export default App;
export const API_PATH = "91.170.211.147:50000";
