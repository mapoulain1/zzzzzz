echo $(tput setaf 1)Stopping server
sudo systemctl stop ZZZZZZ-Server

echo $(tput setaf 2)Building server
tput sgr0
sudo dotnet publish -c Release -o /srv/ZZZZZZ-Server/

sudo systemctl start ZZZZZZ-Server
echo $(tput setaf 2)Starting server

journalctl -u ZZZZZZ-Server.service -ef

