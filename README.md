# ZZZZZZ

Projet tutoré F2 - ZZZZZZ

## Équipe

[Maxime POULAIN - F2](https://gitlab.isima.fr/users/mapoulain1/projects)


[Baptiste VALLET - F2](https://gitlab.isima.fr/users/bavallet/projects)

## Responsable

[Loic YON](https://perso.isima.fr/loic/index.fr.php)

## Serveur

### Docker 

Build container

`docker build --tag local/zzzzzz-server:alpha .`


Run container

`docker run -p 3000:3000 -p 3001:3001/udp -p 3002:3002/udp --rm -it local/zzzzzz-server:alpha`


